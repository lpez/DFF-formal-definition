# Details

Date : 2022-09-26 12:09:35

Directory /home/kjoh/dmf-framework

Total : 32 files,  3394 codes, 322 comments, 368 blanks, all 4084 lines

[Summary](results.md) / Details / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [DMFFramework/BoardState.cs](/DMFFramework/BoardState.cs) | C# | 158 | 3 | 14 | 175 |
| [DMFFramework/ColourState.cs](/DMFFramework/ColourState.cs) | C# | 60 | 1 | 8 | 69 |
| [DMFFramework/Coordinate.cs](/DMFFramework/Coordinate.cs) | C# | 37 | 4 | 4 | 45 |
| [DMFFramework/DMFException.cs](/DMFFramework/DMFException.cs) | C# | 76 | 0 | 8 | 84 |
| [DMFFramework/DMFFramework.csproj](/DMFFramework/DMFFramework.csproj) | XML | 8 | 0 | 3 | 11 |
| [DMFFramework/DropletConfiguration.cs](/DMFFramework/DropletConfiguration.cs) | C# | 148 | 5 | 16 | 169 |
| [DMFFramework/DropletState.cs](/DMFFramework/DropletState.cs) | C# | 54 | 2 | 5 | 61 |
| [DMFFramework/GridTopology.cs](/DMFFramework/GridTopology.cs) | C# | 337 | 87 | 28 | 452 |
| [DMFFramework/HelperFunctions.cs](/DMFFramework/HelperFunctions.cs) | C# | 51 | 6 | 8 | 65 |
| [DMFFramework/Main.cs](/DMFFramework/Main.cs) | C# | 79 | 41 | 16 | 136 |
| [DMFFramework/MergeInProgressTransition.cs](/DMFFramework/MergeInProgressTransition.cs) | C# | 226 | 0 | 16 | 242 |
| [DMFFramework/MergeTransition.cs](/DMFFramework/MergeTransition.cs) | C# | 143 | 0 | 12 | 155 |
| [DMFFramework/Metrics.cs](/DMFFramework/Metrics.cs) | C# | 15 | 0 | 2 | 17 |
| [DMFFramework/MoveTogetherTransition.cs](/DMFFramework/MoveTogetherTransition.cs) | C# | 267 | 7 | 19 | 293 |
| [DMFFramework/MoveTransition.cs](/DMFFramework/MoveTransition.cs) | C# | 155 | 1 | 13 | 169 |
| [DMFFramework/Operation.cs](/DMFFramework/Operation.cs) | C# | 84 | 1 | 10 | 95 |
| [DMFFramework/ProblemFormulation.cs](/DMFFramework/ProblemFormulation.cs) | C# | 157 | 22 | 31 | 210 |
| [DMFFramework/Solution.cs](/DMFFramework/Solution.cs) | C# | 102 | 5 | 12 | 119 |
| [DMFFramework/SplitInProgressTransition.cs](/DMFFramework/SplitInProgressTransition.cs) | C# | 222 | 0 | 16 | 238 |
| [DMFFramework/SplitTransition.cs](/DMFFramework/SplitTransition.cs) | C# | 181 | 0 | 13 | 194 |
| [DMFFramework/Transition.cs](/DMFFramework/Transition.cs) | C# | 311 | 14 | 20 | 345 |
| [DMFFramework/Visualize.py](/DMFFramework/Visualize.py) | Python | 320 | 112 | 86 | 518 |
| [DMFFramework/bin/Debug/net6.0/DMFFramework.deps.json](/DMFFramework/bin/Debug/net6.0/DMFFramework.deps.json) | JSON | 23 | 0 | 0 | 23 |
| [DMFFramework/bin/Debug/net6.0/DMFFramework.runtimeconfig.json](/DMFFramework/bin/Debug/net6.0/DMFFramework.runtimeconfig.json) | JSON | 9 | 0 | 0 | 9 |
| [DMFFramework/obj/DMFFramework.csproj.nuget.dgspec.json](/DMFFramework/obj/DMFFramework.csproj.nuget.dgspec.json) | JSON | 60 | 0 | 0 | 60 |
| [DMFFramework/obj/DMFFramework.csproj.nuget.g.props](/DMFFramework/obj/DMFFramework.csproj.nuget.g.props) | XML | 15 | 0 | 0 | 15 |
| [DMFFramework/obj/DMFFramework.csproj.nuget.g.targets](/DMFFramework/obj/DMFFramework.csproj.nuget.g.targets) | XML | 2 | 0 | 0 | 2 |
| [DMFFramework/obj/Debug/net6.0/.NETCoreApp,Version=v6.0.AssemblyAttributes.cs](/DMFFramework/obj/Debug/net6.0/.NETCoreApp,Version=v6.0.AssemblyAttributes.cs) | C# | 3 | 1 | 1 | 5 |
| [DMFFramework/obj/Debug/net6.0/DMFFramework.AssemblyInfo.cs](/DMFFramework/obj/Debug/net6.0/DMFFramework.AssemblyInfo.cs) | C# | 9 | 9 | 5 | 23 |
| [DMFFramework/obj/Debug/net6.0/DMFFramework.GeneratedMSBuildEditorConfig.editorconfig](/DMFFramework/obj/Debug/net6.0/DMFFramework.GeneratedMSBuildEditorConfig.editorconfig) | Properties | 10 | 0 | 1 | 11 |
| [DMFFramework/obj/Debug/net6.0/DMFFramework.GlobalUsings.g.cs](/DMFFramework/obj/Debug/net6.0/DMFFramework.GlobalUsings.g.cs) | C# | 7 | 1 | 1 | 9 |
| [DMFFramework/obj/project.assets.json](/DMFFramework/obj/project.assets.json) | JSON | 65 | 0 | 0 | 65 |

[Summary](results.md) / Details / [Diff Summary](diff.md) / [Diff Details](diff-details.md)