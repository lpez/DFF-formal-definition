# Diff Details

Date : 2022-09-26 12:09:35

Directory /home/kjoh/dmf-framework

Total : 18 files,  1101 codes, 169 comments, 134 blanks, all 1404 lines

[Summary](results.md) / [Details](details.md) / [Diff Summary](diff.md) / Diff Details

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [DMFFramework/BoardState.cs](/DMFFramework/BoardState.cs) | C# | 54 | 2 | 6 | 62 |
| [DMFFramework/Coordinate.cs](/DMFFramework/Coordinate.cs) | C# | 3 | 0 | 0 | 3 |
| [DMFFramework/DMFException.cs](/DMFFramework/DMFException.cs) | C# | 76 | 0 | 8 | 84 |
| [DMFFramework/DropletConfiguration.cs](/DMFFramework/DropletConfiguration.cs) | C# | 41 | -1 | 1 | 41 |
| [DMFFramework/DropletState.cs](/DMFFramework/DropletState.cs) | C# | 7 | 0 | 1 | 8 |
| [DMFFramework/Main.cs](/DMFFramework/Main.cs) | C# | 79 | 41 | 16 | 136 |
| [DMFFramework/MergeInProgressTransition.cs](/DMFFramework/MergeInProgressTransition.cs) | C# | 65 | 0 | 0 | 65 |
| [DMFFramework/MergeTransition.cs](/DMFFramework/MergeTransition.cs) | C# | 35 | 0 | 0 | 35 |
| [DMFFramework/MoveTogetherTransition.cs](/DMFFramework/MoveTogetherTransition.cs) | C# | 79 | 0 | 0 | 79 |
| [DMFFramework/MoveTransition.cs](/DMFFramework/MoveTransition.cs) | C# | 42 | 0 | 0 | 42 |
| [DMFFramework/Operation.cs](/DMFFramework/Operation.cs) | C# | 29 | 0 | 2 | 31 |
| [DMFFramework/ProblemFormulation.cs](/DMFFramework/ProblemFormulation.cs) | C# | 32 | 17 | 9 | 58 |
| [DMFFramework/Program.cs](/DMFFramework/Program.cs) | C# | -31 | -5 | -6 | -42 |
| [DMFFramework/Solution.cs](/DMFFramework/Solution.cs) | C# | 55 | 3 | 8 | 66 |
| [DMFFramework/SplitInProgressTransition.cs](/DMFFramework/SplitInProgressTransition.cs) | C# | 65 | 0 | 0 | 65 |
| [DMFFramework/SplitTransition.cs](/DMFFramework/SplitTransition.cs) | C# | 51 | 0 | 0 | 51 |
| [DMFFramework/Transition.cs](/DMFFramework/Transition.cs) | C# | 99 | 0 | 3 | 102 |
| [DMFFramework/Visualize.py](/DMFFramework/Visualize.py) | Python | 320 | 112 | 86 | 518 |

[Summary](results.md) / [Details](details.md) / [Diff Summary](diff.md) / Diff Details