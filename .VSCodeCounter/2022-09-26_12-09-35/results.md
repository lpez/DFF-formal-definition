# Summary

Date : 2022-09-26 12:09:35

Directory /home/kjoh/dmf-framework

Total : 32 files,  3394 codes, 322 comments, 368 blanks, all 4084 lines

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| C# | 23 | 2,882 | 210 | 278 | 3,370 |
| Python | 1 | 320 | 112 | 86 | 518 |
| JSON | 4 | 157 | 0 | 0 | 157 |
| XML | 3 | 25 | 0 | 3 | 28 |
| Properties | 1 | 10 | 0 | 1 | 11 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 32 | 3,394 | 322 | 368 | 4,084 |
| DMFFramework | 32 | 3,394 | 322 | 368 | 4,084 |
| DMFFramework/bin | 2 | 32 | 0 | 0 | 32 |
| DMFFramework/bin/Debug | 2 | 32 | 0 | 0 | 32 |
| DMFFramework/bin/Debug/net6.0 | 2 | 32 | 0 | 0 | 32 |
| DMFFramework/obj | 8 | 171 | 11 | 8 | 190 |
| DMFFramework/obj/Debug | 4 | 29 | 11 | 8 | 48 |
| DMFFramework/obj/Debug/net6.0 | 4 | 29 | 11 | 8 | 48 |

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)