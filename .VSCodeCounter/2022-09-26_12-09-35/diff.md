# Diff Summary

Date : 2022-09-26 12:09:35

Directory /home/kjoh/dmf-framework

Total : 18 files,  1101 codes, 169 comments, 134 blanks, all 1404 lines

[Summary](results.md) / [Details](details.md) / Diff Summary / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| C# | 17 | 781 | 57 | 48 | 886 |
| Python | 1 | 320 | 112 | 86 | 518 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 18 | 1,101 | 169 | 134 | 1,404 |
| DMFFramework | 18 | 1,101 | 169 | 134 | 1,404 |

[Summary](results.md) / [Details](details.md) / Diff Summary / [Diff Details](diff-details.md)