# Details

Date : 2022-09-13 22:31:38

Directory /home/kjoh/dmf-framework

Total : 30 files,  2293 codes, 153 comments, 234 blanks, all 2680 lines

[Summary](results.md) / Details / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [DMFFramework/BoardState.cs](/DMFFramework/BoardState.cs) | C# | 104 | 1 | 8 | 113 |
| [DMFFramework/ColourState.cs](/DMFFramework/ColourState.cs) | C# | 60 | 1 | 8 | 69 |
| [DMFFramework/Coordinate.cs](/DMFFramework/Coordinate.cs) | C# | 34 | 4 | 4 | 42 |
| [DMFFramework/DMFFramework.csproj](/DMFFramework/DMFFramework.csproj) | XML | 8 | 0 | 3 | 11 |
| [DMFFramework/DropletConfiguration.cs](/DMFFramework/DropletConfiguration.cs) | C# | 107 | 6 | 15 | 128 |
| [DMFFramework/DropletState.cs](/DMFFramework/DropletState.cs) | C# | 47 | 2 | 4 | 53 |
| [DMFFramework/GridTopology.cs](/DMFFramework/GridTopology.cs) | C# | 337 | 87 | 28 | 452 |
| [DMFFramework/HelperFunctions.cs](/DMFFramework/HelperFunctions.cs) | C# | 51 | 6 | 8 | 65 |
| [DMFFramework/MergeInProgressTransition.cs](/DMFFramework/MergeInProgressTransition.cs) | C# | 161 | 0 | 16 | 177 |
| [DMFFramework/MergeTransition.cs](/DMFFramework/MergeTransition.cs) | C# | 108 | 0 | 12 | 120 |
| [DMFFramework/Metrics.cs](/DMFFramework/Metrics.cs) | C# | 15 | 0 | 2 | 17 |
| [DMFFramework/MoveTogetherTransition.cs](/DMFFramework/MoveTogetherTransition.cs) | C# | 188 | 7 | 19 | 214 |
| [DMFFramework/MoveTransition.cs](/DMFFramework/MoveTransition.cs) | C# | 113 | 1 | 13 | 127 |
| [DMFFramework/Operation.cs](/DMFFramework/Operation.cs) | C# | 55 | 1 | 8 | 64 |
| [DMFFramework/ProblemFormulation.cs](/DMFFramework/ProblemFormulation.cs) | C# | 125 | 5 | 22 | 152 |
| [DMFFramework/Program.cs](/DMFFramework/Program.cs) | C# | 31 | 5 | 6 | 42 |
| [DMFFramework/Solution.cs](/DMFFramework/Solution.cs) | C# | 47 | 2 | 4 | 53 |
| [DMFFramework/SplitInProgressTransition.cs](/DMFFramework/SplitInProgressTransition.cs) | C# | 157 | 0 | 16 | 173 |
| [DMFFramework/SplitTransition.cs](/DMFFramework/SplitTransition.cs) | C# | 130 | 0 | 13 | 143 |
| [DMFFramework/Transition.cs](/DMFFramework/Transition.cs) | C# | 212 | 14 | 17 | 243 |
| [DMFFramework/bin/Debug/net6.0/DMFFramework.deps.json](/DMFFramework/bin/Debug/net6.0/DMFFramework.deps.json) | JSON | 23 | 0 | 0 | 23 |
| [DMFFramework/bin/Debug/net6.0/DMFFramework.runtimeconfig.json](/DMFFramework/bin/Debug/net6.0/DMFFramework.runtimeconfig.json) | JSON | 9 | 0 | 0 | 9 |
| [DMFFramework/obj/DMFFramework.csproj.nuget.dgspec.json](/DMFFramework/obj/DMFFramework.csproj.nuget.dgspec.json) | JSON | 60 | 0 | 0 | 60 |
| [DMFFramework/obj/DMFFramework.csproj.nuget.g.props](/DMFFramework/obj/DMFFramework.csproj.nuget.g.props) | XML | 15 | 0 | 0 | 15 |
| [DMFFramework/obj/DMFFramework.csproj.nuget.g.targets](/DMFFramework/obj/DMFFramework.csproj.nuget.g.targets) | XML | 2 | 0 | 0 | 2 |
| [DMFFramework/obj/Debug/net6.0/.NETCoreApp,Version=v6.0.AssemblyAttributes.cs](/DMFFramework/obj/Debug/net6.0/.NETCoreApp,Version=v6.0.AssemblyAttributes.cs) | C# | 3 | 1 | 1 | 5 |
| [DMFFramework/obj/Debug/net6.0/DMFFramework.AssemblyInfo.cs](/DMFFramework/obj/Debug/net6.0/DMFFramework.AssemblyInfo.cs) | C# | 9 | 9 | 5 | 23 |
| [DMFFramework/obj/Debug/net6.0/DMFFramework.GeneratedMSBuildEditorConfig.editorconfig](/DMFFramework/obj/Debug/net6.0/DMFFramework.GeneratedMSBuildEditorConfig.editorconfig) | Properties | 10 | 0 | 1 | 11 |
| [DMFFramework/obj/Debug/net6.0/DMFFramework.GlobalUsings.g.cs](/DMFFramework/obj/Debug/net6.0/DMFFramework.GlobalUsings.g.cs) | C# | 7 | 1 | 1 | 9 |
| [DMFFramework/obj/project.assets.json](/DMFFramework/obj/project.assets.json) | JSON | 65 | 0 | 0 | 65 |

[Summary](results.md) / Details / [Diff Summary](diff.md) / [Diff Details](diff-details.md)