# Summary

Date : 2022-09-13 22:31:38

Directory /home/kjoh/dmf-framework

Total : 30 files,  2293 codes, 153 comments, 234 blanks, all 2680 lines

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| C# | 22 | 2,101 | 153 | 230 | 2,484 |
| JSON | 4 | 157 | 0 | 0 | 157 |
| XML | 3 | 25 | 0 | 3 | 28 |
| Properties | 1 | 10 | 0 | 1 | 11 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 30 | 2,293 | 153 | 234 | 2,680 |
| DMFFramework | 30 | 2,293 | 153 | 234 | 2,680 |
| DMFFramework/bin | 2 | 32 | 0 | 0 | 32 |
| DMFFramework/bin/Debug | 2 | 32 | 0 | 0 | 32 |
| DMFFramework/bin/Debug/net6.0 | 2 | 32 | 0 | 0 | 32 |
| DMFFramework/obj | 8 | 171 | 11 | 8 | 190 |
| DMFFramework/obj/Debug | 4 | 29 | 11 | 8 | 48 |
| DMFFramework/obj/Debug/net6.0 | 4 | 29 | 11 | 8 | 48 |

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)