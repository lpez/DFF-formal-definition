using System;

namespace DMFFramework
{
    class ProblemFormulation
    {
        public GridTopology GridTopology { get; set; }
        public List<DropletState> DropletStates { get; set; }
        public int[] Colours { get; set; }
        public Dictionary<int, DropletState> DropletIdToDropletState { get; set; }
        public List<Operation> Operations { get; set; }
        public Dictionary<Operation, List<Operation>> OperationToNextOperations { get; set; }
        public Dictionary<Operation, List<Operation>> OperationToPreviousOperations { get; set; }

        public ProblemFormulation(string operationGraphInput, GridTopology gridTopology)
        {
            GridTopology = gridTopology;
            InitDictionaries(operationGraphInput);

            //foreach (Operation test in OperationToNextOperations.Keys)
            //{
            //    Console.WriteLine("Operation:  " + test.Print());
            //    foreach (Operation ins in OperationToPreviousOperations[test])
            //    {
            //        Console.WriteLine("in:  " + ins.Print());
            //    }
            //
            //    foreach (Operation outs in OperationToNextOperations[test])
            //    {
            //        Console.WriteLine("out:  " + outs.Print());
            //    }
            //}
        }


        public BoardState GetStartingState(GridTopology gridTopology, HelperFunctions helperFunctions)
        {
            // Initialize an unpolluted board
            var pollutedBoard = new List<List<int>>(gridTopology.Width);
            for (int i = 0; i < gridTopology.Width; i++)
            {
                pollutedBoard.Add( Enumerable.Range(0, gridTopology.Height).Select(x => 0).ToList() );
            }
            // Initialize an unpopulated list of initialize droplet states
            var startingDropletStates = new List<DropletState>();
            
            foreach (Operation operation in Operations)
            {
                // In an operation has no predecessors
                if ( OperationToPreviousOperations[operation].Count() == 0 )
                {
                    foreach (DropletState ds in operation.DropletStatesIn)
                    {
                        startingDropletStates.Add(ds);
                        foreach (Coordinate cell in gridTopology.NO(ds.Coordinate, ds.Volume))
                        {
                            pollutedBoard[cell.X][cell.Y] = ds.Colour;
                        }
                    }
                }
            }
            var colourState = new ColourState(gridTopology, pollutedBoard);
            var dropletConfiguration = new DropletConfiguration(gridTopology, startingDropletStates);
            return new BoardState(gridTopology, colourState, dropletConfiguration, helperFunctions);
        }

// INITIALIZATION METHODS
        private void InitDictionaries(string operationGraphInput)
        {
            var graphInput = File.ReadAllText(operationGraphInput);

            string[] split = graphInput.Split("|");

            string[] dropletStateStrings = split[0].Split("\n", StringSplitOptions.RemoveEmptyEntries);

            // Initialize DropletStates list and DropletIdToDropletState dictionary
            DropletStates = 
            (
                from dropletStateString in dropletStateStrings
                select new DropletState(dropletStateString, GridTopology)
            ).ToList();

            Colours =
            (
                from dropletState in DropletStates
                select dropletState.Colour
            ).Distinct().ToArray();

            DropletIdToDropletState = new Dictionary<int, DropletState>();
            DropletStates.ForEach(dropletState => DropletIdToDropletState.Add(dropletState.Id, dropletState));

            // Initialize Operations list
            Operations = new List<Operation>();
            string[] operationInfoStrings = split[1].Split("\n", StringSplitOptions.RemoveEmptyEntries);
            string[] dropletStateIdsToOperationIds = split[2].Split("\n", StringSplitOptions.RemoveEmptyEntries);
            string[] operationIdToDropletStateIds = split[3].Split("\n", StringSplitOptions.RemoveEmptyEntries);



            var operationIdsToInNeighbourDropletIds = new Dictionary<int, List<int>>();
            foreach (string relation in dropletStateIdsToOperationIds)
            {
                string[] relationSplit = relation.Split(" -> ");

                int operationId;
                List<int> inNeighbourDropletIds;

                // Try to parse and add
                try
                {
                    operationId = Int32.Parse(relationSplit[1]);
                    inNeighbourDropletIds = 
                    (
                        from dropletId in relationSplit[0].Split("; ")
                        where !dropletId.Equals("_")
                        select Int32.Parse(dropletId)
                    ).ToList();
                } catch(System.FormatException e)
                {
                    throw new System.FormatException("Wrong operation graph input format");
                }

                if ( !operationIdsToInNeighbourDropletIds.TryAdd(operationId, inNeighbourDropletIds) )
                {
                    Console.WriteLine("Could not add to dictionary");
                }               
            }

            var operationIdsToOutNeighbourDropletIds = new Dictionary<int, List<int>>();
            foreach (string relation in operationIdToDropletStateIds)
            {
                string[] relationSplit = relation.Split(" -> ");
                
                int operationId;
                List<int> outNeighbourDropletIds;

                // Try to parse and add
                try
                {
                    operationId = Int32.Parse(relationSplit[0]);
                    outNeighbourDropletIds = 
                    (
                        from dropletId in relationSplit[1].Split("; ")
                        where !dropletId.Equals("_")
                        select Int32.Parse(dropletId)
                    ).ToList();
                } catch(System.FormatException e)
                {
                    throw new System.FormatException("Wrong operation graph input format");
                }

                if ( !operationIdsToOutNeighbourDropletIds.TryAdd(operationId, outNeighbourDropletIds) )
                {
                    Console.WriteLine("Could not add to dictionary");
                }
            }

            
            foreach (string operationInfoString in operationInfoStrings)
            {
                var operationInfo = new OperationInfo(operationInfoString);

                List<int> inNeighbourIds = operationIdsToInNeighbourDropletIds[ operationInfo.i ];
                List<int> outNeighbourIds = operationIdsToOutNeighbourDropletIds[ operationInfo.i ];

                Operations.Add
                (
                    new Operation
                    (
                        inNeighbourIds.Select(dropletId => DropletIdToDropletState[dropletId]).ToList(),
                        operationInfo,
                        outNeighbourIds.Select(dropletId => DropletIdToDropletState[dropletId]).ToList()
                    )
                );
            }

            // Initialize OperationToNextOperations and OperationToPreviousOperations
            OperationToNextOperations = new Dictionary<Operation, List<Operation>>();
            OperationToPreviousOperations = new Dictionary<Operation, List<Operation>>();

            foreach (Operation operation in Operations)
            {
                var nextOperations = new List<Operation>();
                var previousOperations = new List<Operation>();
                foreach (Operation otherOperation in Operations)
                {
                    if (operation != otherOperation)
                    {;
                        foreach (DropletState otherDropletState in otherOperation.DropletStatesIn)
                        {
                            if (operation.DropletStatesOut.Contains(otherDropletState))
                            {
                                nextOperations.Add(otherOperation);
                            }
                        }

                        foreach (DropletState otherDropletState in otherOperation.DropletStatesOut)
                        {
                            if (operation.DropletStatesIn.Contains(otherDropletState))
                            {
                                previousOperations.Add(otherOperation);
                            }
                        }
                    }
                }
                OperationToNextOperations.Add(operation, nextOperations);
                OperationToPreviousOperations.Add(operation, previousOperations);
            }
        }

    }
}