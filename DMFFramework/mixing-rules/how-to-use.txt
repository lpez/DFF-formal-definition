Unless otherwise specified, E[0][c1] == c1 and E[c1][c1] == c1 and E[c1][c2] == 0.
To specify a new rule that mixing c1 and c2 should give c3 
we add a line of the following format:

c1, c2, c3

The tabula should be symmetric so such a line should imply E[c1][c2] == c3
and E[c2][c1] == c3. 

The format should first completely specify the colour classes. 
Following should be a list of rules we wish to alter.
0 is always in the colour class list. This will help us weed out cases
where (*) does not hold.
Check 'ruleset1.txt' to see the format.