using System;

namespace DMFFramework
{
    class SplitTransition : ITransition
    {
        DropletState DropletBeforeMove;
        DropletState Droplet1AfterMove;
        DropletState Droplet2AfterMove;

        public SplitTransition(DropletState dropletBeforeMore, DropletState droplet1AfterMove, DropletState droplet2AfterMove)
        {
            DropletBeforeMove = dropletBeforeMore;
            Droplet1AfterMove = droplet1AfterMove;
            Droplet2AfterMove = droplet2AfterMove;
        }

        public List<DropletState> GetDropletStatesBefore()
        {
            return new List<DropletState>(1) { DropletBeforeMove };
        }

        public List<DropletState> GetDropletStatesAfter()
        {
            return new List<DropletState>(2) { Droplet1AfterMove, Droplet2AfterMove };
        }

        public string WriteTransitionToTxtFormat()
        {
            string returnString = String.Format
            (
                "6#{0} | _ => {1} | {2}",
                DropletBeforeMove.PrintDropletState(),
                Droplet1AfterMove.PrintDropletState(),
                Droplet2AfterMove.PrintDropletState()
            );
            return returnString;
        }

        public bool IsFeasible( GridTopology gridTopology, BoardState boardStateBefore, HelperFunctions helperFunctions, IMetric metric )
        {
            if
            (
                IdsAreCorrect()
                && VolumesAreCorrect()
                && NewPositionsAreDistinct()
                && MergeCellsAreAdjacent(gridTopology)
                && ColoursAreCorrect(helperFunctions)
                && NewPositionsAreAllowedToMoveToRegardsToPollution(gridTopology, boardStateBefore, helperFunctions)
            )
            {
                return true;
            }
            return false;   
        }

        private bool IdsAreCorrect()
        {
            if
            (
                DropletBeforeMove.Id != Droplet1AfterMove.Id
                && DropletBeforeMove.Id != Droplet2AfterMove.Id
                && Droplet1AfterMove.Id != Droplet2AfterMove.Id
            )
            {
                return true;
            }
            string errorMessage = 
            String.Format
            (
                "Ids are not correct in the following SplitTransition: \n {0}",
                this.WriteTransitionToTxtFormat()
            );
            Console.WriteLine(errorMessage);
            return false;
        }

        private bool VolumesAreCorrect()
        {
            if (DropletBeforeMove.Volume == Droplet1AfterMove.Volume + Droplet2AfterMove.Volume)
            {
                return true;
            }
            string errorMessage = 
            String.Format
            (
                "Volumes are not correct in the following SplitTransition: \n {0}",
                this.WriteTransitionToTxtFormat()
            );
            Console.WriteLine(errorMessage);
            return false;
        }

        private bool NewPositionsAreDistinct()
        {
            if ( Droplet1AfterMove.Coordinate.OnTopOf( Droplet2AfterMove.Coordinate ) )
            {
                string errorMessage = 
                String.Format
                (
                    "New positions are not distinct in the following SplitTransition: \n {0}",
                    this.WriteTransitionToTxtFormat()
                );
                Console.WriteLine(errorMessage);
                return false;
            }
            return true;
        }

        private bool MergeCellsAreAdjacent( GridTopology gridTopology )
        {
            if 
            ( 
                gridTopology.N(DropletBeforeMove.Coordinate, Droplet1AfterMove.Volume).Contains(Droplet1AfterMove.Coordinate)
                && gridTopology.N(DropletBeforeMove.Coordinate, Droplet2AfterMove.Volume).Contains(Droplet2AfterMove.Coordinate)
            )
            {
                return true;
            }
            string errorMessage = 
            String.Format
            (
                "Split cells are not adjacent in the following SplitTransition: \n {0}",
                this.WriteTransitionToTxtFormat()
            );
            Console.WriteLine(errorMessage);
            return false;
        }

        private bool ColoursAreCorrect( HelperFunctions helperFunctions )
        {
            if 
            (
                Droplet1AfterMove.Colour == DropletBeforeMove.Colour
                && Droplet2AfterMove.Colour == DropletBeforeMove.Colour
            )
            {
                return true;
            }
            string errorMessage = 
            String.Format
            (
                "Colours are not correct in the following SplitTransition: \n {0}",
                this.WriteTransitionToTxtFormat()
            );
            Console.WriteLine(errorMessage);
            return false;
        }

        private bool NewPositionsAreAllowedToMoveToRegardsToPollution( GridTopology gridTopology, BoardState boardStateBefore, HelperFunctions helperFunctions )
        {
            foreach (Coordinate cell in gridTopology.NO(Droplet1AfterMove.Coordinate, Droplet1AfterMove.Volume))
            {
                if 
                (
                    !( boardStateBefore.ColourState.GetColour(cell) == 0 )
                    && !( boardStateBefore.ColourState.GetColour(cell) == DropletBeforeMove.Colour )
                )
                {
                    string errorMessage = 
                    String.Format
                    (
                        "Position of droplet {0} is polluted initially in the following SplitTransition: \n {1}",
                        Droplet1AfterMove.Id,
                        this.WriteTransitionToTxtFormat()
                    );
                    Console.WriteLine(errorMessage);
                    return false;
                }
            }

            foreach (Coordinate cell in gridTopology.NO(Droplet2AfterMove.Coordinate, Droplet2AfterMove.Volume))
            {
                if 
                (
                    !( boardStateBefore.ColourState.GetColour(cell) == 0 )
                    && !( boardStateBefore.ColourState.GetColour(cell) == DropletBeforeMove.Colour )
                )
                {
                    string errorMessage = 
                    String.Format
                    (
                        "Position of droplet {0} is polluted initially in the following SplitTransition: \n {1}",
                        Droplet2AfterMove.Id,
                        this.WriteTransitionToTxtFormat()
                    );
                    Console.WriteLine(errorMessage);
                    return false;
                }
            }
            return true;
        }
    }
}