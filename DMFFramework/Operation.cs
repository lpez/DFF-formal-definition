using System;

namespace DMFFramework
{
    interface IOperationExecutionSequence
    {
        public Operation Operation { get; set; }
        public List<ITransition> MinorTransitions { get; set; }
        public bool IsFeasible();
    }

    class MoveOperationExecutionSequence : IOperationExecutionSequence
    {
        public Operation Operation { get; set; }
        public List<ITransition> MinorTransitions { get; set; }
        
        public MoveOperationExecutionSequence(Operation operation)
        {
            Operation = operation;
            MinorTransitions = new List<ITransition>();
        }

        public bool IsFeasible()
        {
            var firstDroplet = MinorTransitions[0].GetDropletStatesBefore()[0];
            var LastTransitionIndex = MinorTransitions.Count() - 1;
            
            for (int i = 0; i < MinorTransitions.Count(); i++)
            {
                var minorTransition = MinorTransitions[i];
                // Transitions should have correct types
                if ( !(minorTransition is MoveTransition) )
                {
                    var errorMessage = String.Format
                    (
                        "The following operation is not carried out: \n {0}",
                        Operation.Print()
                    );
                    Console.WriteLine(errorMessage);
                    return false;
                }

                // Transitions should have correct ids
                if 
                ( i == LastTransitionIndex 
                  && firstDroplet.Id == minorTransition.GetDropletStatesAfter()[0].Id )
                {
                    var errorMessage = String.Format
                    (
                        "The following operation does not have correct ids: \n {0}",
                        Operation.Print()
                    );
                    Console.WriteLine(errorMessage);
                    Console.WriteLine("Members in the following minor transition should have distinct ids: /n {0}", minorTransition.WriteTransitionToTxtFormat());
                    return false;
                }
                else if ( firstDroplet.Id != minorTransition.GetDropletStatesBefore()[0].Id )
                {
                    var errorMessage = String.Format
                    (
                        "The following operation does not have correct ids: \n {0}",
                        Operation.Print()
                    );
                    Console.WriteLine(errorMessage);
                    return false;
                }

            }
            // Transitions should have correct volumes
                // Is almost inherently taken care of by the structure of the minor transition sequence
                // and the fact that each minor transition has a feasibility check
            if ( Operation.DropletStatesIn[0].Volume != firstDroplet.Volume )
            {
                var errorMessage = String.Format
                (
                    "The following operation does not have correct volumes: \n {0}",
                    Operation.Print()
                );
                Console.WriteLine(errorMessage);
                return false;
            }
            if ( Operation.DropletStatesOut[0].Volume != MinorTransitions[LastTransitionIndex].GetDropletStatesAfter()[0].Volume )
            {
                var errorMessage = String.Format
                (
                    "The following operation does not have correct volumes: \n {0}",
                    Operation.Print()
                );
                Console.WriteLine(errorMessage);
                return false;
            }

            // Transition should have correct colours
                // Is almost inherently taken care of by the structure of the minor transition sequence
                // and the fact that each minor transition has a feasibility check
            if ( Operation.DropletStatesIn[0].Colour != firstDroplet.Colour )
            {
                var errorMessage = String.Format
                (
                    "The following operation does not have correct colours: \n {0}",
                    Operation.Print()
                );
                Console.WriteLine(errorMessage);
                return false;
            }
            if ( Operation.DropletStatesOut[0].Colour != MinorTransitions[LastTransitionIndex].GetDropletStatesAfter()[0].Colour )
            {
                var errorMessage = String.Format
                (
                    "The following operation does not have correct colours: \n {0}",
                    Operation.Print()
                );
                Console.WriteLine(errorMessage);
                return false;
            }
            return true;
        }
    }

    class MergeOperationExecutionSequence : IOperationExecutionSequence
    {
        public Operation Operation { get; set; }
        public List<ITransition> MinorTransitions { get; set; }
        
        public MergeOperationExecutionSequence(Operation operation)
        {
            Operation = operation;
            MinorTransitions = new List<ITransition>();
        }
        public bool IsFeasible()
        {
            var LastTransitionIndex = MinorTransitions.Count() - 1;

            var droplet0Before = MinorTransitions[0].GetDropletStatesBefore()[0];
            var droplet1Before = MinorTransitions[0].GetDropletStatesBefore()[1];
            var dropletAfter = MinorTransitions[LastTransitionIndex].GetDropletStatesAfter()[0];

            // Transition should have correct ids
                    // Is almost inherently taken care of by the structure of the minor transition sequence
                    // and the fact that each minor transition has a feasibility check
            if
            (
                Operation.DropletStatesIn.Where( dropletState => dropletState.Id == droplet0Before.Id ).Count() == 0
                || Operation.DropletStatesIn.Where( dropletState => dropletState.Id == droplet1Before.Id ).Count() == 0
                || dropletAfter.Id != Operation.DropletStatesOut[0].Id
            )
            {
                var errorMessage = String.Format
                (
                    "The ids are not correct in for the following operation: \n {0}",
                    Operation.Print()
                );
                Console.WriteLine(errorMessage);
                return false;   
            }

            // Transition should have correct volumes
                    // Is almost inherently taken care of by the structure of the minor transition sequence
                    // and the fact that each minor transition has a feasibility check
            if
            (
                Operation.DropletStatesIn.Where( dropletState => dropletState.Volume == droplet0Before.Volume ).Count() == 0
                || Operation.DropletStatesIn.Where( dropletState => dropletState.Volume == droplet1Before.Volume ).Count() == 0
                || dropletAfter.Volume != Operation.DropletStatesOut[0].Volume
            )
            {
                var errorMessage = String.Format
                (
                    "The volumes are not correct in for the following operation: \n {0}",
                    Operation.Print()
                );
                Console.WriteLine(errorMessage);
                return false;   
            }

            // Transition should have correct colours
            if
            (
                Operation.DropletStatesIn.Where( dropletState => dropletState.Colour == droplet0Before.Colour ).Count() == 0
                || Operation.DropletStatesIn.Where( dropletState => dropletState.Colour == droplet1Before.Colour ).Count() == 0
                || dropletAfter.Colour != Operation.DropletStatesOut[0].Colour
            )
            {
                var errorMessage = String.Format
                (
                    "The colours are not correct in for the following operation: \n {0}",
                    Operation.Print()
                );
                Console.WriteLine(errorMessage);
                return false;   
            }

            // Transitions should have correct types

            // We have three cases:

            // 1. A single 'MergeTransition'
            // 2. Several 'MoveTogetherTransition' followed by a 'MergeTransition'
            // 3. Several 'MoveTogetherTransition' followed by several 'MergeInProgressTransition' followed by a 'MergeTransition' 


            // Makes sure the last minor transition is a 'MergeTransition'
            if ( !(MinorTransitions[LastTransitionIndex] is MergeTransition) )
            {
                var errorMessage = String.Format
                (
                    "The following operation transition does not end with a merge transition: \n {0}",
                    Operation.Print()
                );
                Console.WriteLine(errorMessage);
                return false;
            }

            // Case 1:
            if ( MinorTransitions.Count() == 1 )
            {
                return true;
            }
            // Cases 2 and 3:
            bool remainingTransitionsMustBeMergeInTransition = false;
            for (int i = 0; i < LastTransitionIndex; i++)
            {
                var minorTransition = MinorTransitions[i];
                if 
                ( 
                    minorTransition is MoveTogetherTransition && !remainingTransitionsMustBeMergeInTransition 
                )
                {
                    continue;
                }
                else if ( minorTransition is MergeInProgressTransition )
                {
                    remainingTransitionsMustBeMergeInTransition = true;
                    continue;
                }

                var errorMessage = String.Format
                (
                    "The execution of the following operation does not have the proper structure: \n {0}",
                    Operation.Print()
                );
                Console.WriteLine(errorMessage);
                return false;
            }
            // We must perform extra checks to make sure colours are correct
            // ---- Should optimize later by including into the loop above
            for (int i = 0; i < LastTransitionIndex; i++)
            {
                var minorTransition = MinorTransitions[i];
                var dropletStatesBefore = minorTransition.GetDropletStatesBefore();
                var dropletStatesAfter = minorTransition.GetDropletStatesAfter();

                if 
                ( 
                    ( dropletStatesBefore[0].Colour != dropletAfter.Colour
                    && dropletStatesBefore[0].Colour != droplet0Before.Colour )
                    ||
                    ( dropletStatesBefore[0].Colour == dropletAfter.Colour
                    && dropletStatesBefore[0].Colour != dropletStatesAfter[0].Colour )
                )
                {
                    var errorMessage = String.Format
                    (
                        "The colours are not correct in operation: \n {0} \n for the following minor transition:",
                        Operation.Print()
                    );
                    Console.WriteLine(errorMessage);
                    Console.WriteLine(minorTransition.WriteTransitionToTxtFormat());
                    return false;
                }

                if 
                ( 
                    ( dropletStatesBefore[1].Colour != dropletAfter.Colour
                    && dropletStatesBefore[1].Colour != droplet1Before.Colour )
                    ||
                    ( dropletStatesBefore[1].Colour == dropletAfter.Colour
                    && dropletStatesBefore[1].Colour != dropletStatesAfter[1].Colour )
                )
                {
                    var errorMessage = String.Format
                    (
                        "The colours are not correct in operation \n : {0} \n for the following minor transition:",
                        Operation.Print()
                    );
                    Console.WriteLine(errorMessage);
                    Console.WriteLine(minorTransition.WriteTransitionToTxtFormat());
                    return false;
                }
            }
            return true;
        }


    }

    class MixOperationExecutionSequence : IOperationExecutionSequence
    {
        public Operation Operation { get; set; }
        public List<ITransition> MinorTransitions { get; set; }
        
        public MixOperationExecutionSequence(Operation operation)
        {
            Operation = operation;
            MinorTransitions = new List<ITransition>();
        }
        public bool IsFeasible()
        {
            var firstDroplet = MinorTransitions[0].GetDropletStatesBefore()[0];
            var LastTransitionIndex = MinorTransitions.Count() - 1;
            
            for (int i = 0; i < MinorTransitions.Count(); i++)
            {
                var minorTransition = MinorTransitions[i];
                // Transitions should have correct types
                if ( !(minorTransition is MoveTransition) )
                {
                    var errorMessage = String.Format
                    (
                        "The following operation is not carried out: \n {0}",
                        Operation.Print()
                    );
                    Console.WriteLine(errorMessage);
                    return false;
                }

                // Transitions should have correct ids
                if 
                ( i == LastTransitionIndex 
                  && firstDroplet.Id == minorTransition.GetDropletStatesAfter()[0].Id )
                {
                    var errorMessage = String.Format
                    (
                        "The following operation does not have correct ids: \n {0}",
                        Operation.Print()
                    );
                    Console.WriteLine(errorMessage);
                    return false;
                }
                else if ( firstDroplet.Id != minorTransition.GetDropletStatesAfter()[0].Id )
                {
                    var errorMessage = String.Format
                    (
                        "The following operation does not have correct ids: \n {0}",
                        Operation.Print()
                    );
                    Console.WriteLine(errorMessage);
                    return false;
                }

                // Transition can not perform any 180 degree turns
                var firstCoordinate = MinorTransitions[i].GetDropletStatesBefore()[0].Coordinate;
                var secondCoordinate = MinorTransitions[i+1].GetDropletStatesBefore()[0].Coordinate;
                if (firstCoordinate.OnTopOf(secondCoordinate))
                {
                    var errorMessage = String.Format
                    (
                        "The following mix operation performs a 180 degree turn: \n {0}",
                        Operation.Print()
                    );
                    Console.WriteLine(errorMessage);
                    return false;
                }
            }
            // Transitions should have correct volumes
                // Is almost inherently taken care of by the structure of the minor transition sequence
                // and the fact that each minor transition has a feasibility check
            if ( Operation.DropletStatesIn[0].Volume != firstDroplet.Volume )
            {
                var errorMessage = String.Format
                (
                    "The following operation does not have correct volumes: \n {0}",
                    Operation.Print()
                );
                Console.WriteLine(errorMessage);
                return false;
            }
            if ( Operation.DropletStatesOut[0].Volume != MinorTransitions[LastTransitionIndex].GetDropletStatesAfter()[0].Volume )
            {
                var errorMessage = String.Format
                (
                    "The following operation does not have correct volumes: \n {0}",
                    Operation.Print()
                );
                Console.WriteLine(errorMessage);
                return false;
            }

            // Transition should have correct colours
                // Is almost inherently taken care of by the structure of the minor transition sequence
                // and the fact that each minor transition has a feasibility check
            if ( Operation.DropletStatesIn[0].Colour != firstDroplet.Colour )
            {
                var errorMessage = String.Format
                (
                    "The following operation does not have correct colours: \n {0}",
                    Operation.Print()
                );
                Console.WriteLine(errorMessage);
                return false;
            }
            if ( Operation.DropletStatesOut[0].Colour != MinorTransitions[LastTransitionIndex].GetDropletStatesAfter()[0].Colour )
            {
                var errorMessage = String.Format
                (
                    "The following operation does not have correct colours: \n {0}",
                    Operation.Print()
                );
                Console.WriteLine(errorMessage);
                return false;
            }
            return true;
        }
    }

    class SplitOperationExecutionSequence : IOperationExecutionSequence
    {
        public Operation Operation { get; set; }
        public List<ITransition> MinorTransitions { get; set; }
        
        public SplitOperationExecutionSequence(Operation operation)
        {
            Operation = operation;
            MinorTransitions = new List<ITransition>();
        }
        public bool IsFeasible()
        {
            var LastTransitionIndex = MinorTransitions.Count() - 1;

            var dropletBefore = MinorTransitions[0].GetDropletStatesBefore()[0];
            var droplet0After = MinorTransitions[0].GetDropletStatesAfter()[0];
            var droplet1After = MinorTransitions[LastTransitionIndex].GetDropletStatesAfter()[1];

            // Transition should have correct ids
                    // Is almost inherently taken care of by the structure of the minor transition sequence
                    // and the fact that each minor transition has a feasibility check
            if
            (
                Operation.DropletStatesOut.Where( dropletState => dropletState.Id == droplet0After.Id ).Count() == 0
                || Operation.DropletStatesOut.Where( dropletState => dropletState.Id == droplet1After.Id ).Count() == 0
                || dropletBefore.Id != Operation.DropletStatesIn[0].Id
            )
            {
                var errorMessage = String.Format
                (
                    "The ids are not correct in for the following operation: \n {0}",
                    Operation.Print()
                );
                Console.WriteLine(errorMessage);
                return false;   
            }

            // Transition should have correct volumes
                    // Is almost inherently taken care of by the structure of the minor transition sequence
                    // and the fact that each minor transition has a feasibility check
            if
            (
                Operation.DropletStatesOut.Where( dropletState => dropletState.Volume == droplet0After.Volume ).Count() == 0
                || Operation.DropletStatesOut.Where( dropletState => dropletState.Volume == droplet1After.Volume ).Count() == 0
                || dropletBefore.Volume != Operation.DropletStatesIn[0].Volume
            )
            {
                var errorMessage = String.Format
                (
                    "The volumes are not correct in for the following operation: \n {0}",
                    Operation.Print()
                );
                Console.WriteLine(errorMessage);
                return false;   
            }

            // Transition should have correct colours
            if
            (
                Operation.DropletStatesOut.Where( dropletState => dropletState.Colour == droplet0After.Colour ).Count() == 0
                || Operation.DropletStatesOut.Where( dropletState => dropletState.Colour == droplet1After.Colour ).Count() == 0
                || dropletBefore.Colour != Operation.DropletStatesIn[0].Colour
            )
            {
                var errorMessage = String.Format
                (
                    "The colours are not correct in for the following operation: \n {0}",
                    Operation.Print()
                );
                Console.WriteLine(errorMessage);
                return false;   
            }

            // Transitions should have correct types

            // We have two cases:

            // 1. A single 'SplitTransition'
            // 2. A single 'SplitTransition' followed by several 'SplitInProgressTransitions' '
            
            // Case 1:
            if ( !(MinorTransitions[0] is SplitTransition) )
            {
                var errorMessage = String.Format
                (
                    "The following operation transition is not carried out: \n {0}",
                    Operation.Print()
                );
                Console.WriteLine(errorMessage);
                return false;
            }

            // Case 2: 
            if ( MinorTransitions.Count() > 1 )
            {
                for (int i = 1; i <= LastTransitionIndex; i++)
                {
                    var minorTransition = MinorTransitions[i];

                    if ( !(minorTransition is SplitInProgressTransition) )
                    {
                        var errorMessage = String.Format
                        (
                            "The following operation transition is not carried out: \n {0}",
                            Operation.Print()
                        );
                        Console.WriteLine(errorMessage);
                        return false;
                    }
                }
            }
            return true;
        }
    }

    class DispenseOperationExecutionSequence : IOperationExecutionSequence
    {
        public Operation Operation { get; set; }
        public List<ITransition> MinorTransitions { get; set; }
        
        public DispenseOperationExecutionSequence(Operation operation)
        {
            Operation = operation;
            MinorTransitions = new List<ITransition>();
        }
        public bool IsFeasible()
        {
            return true;
        }
    }

    class HeatOperationExecutionSequence : IOperationExecutionSequence
    {
        public Operation Operation { get; set; }
        public List<ITransition> MinorTransitions { get; set; }
        
        public HeatOperationExecutionSequence(Operation operation)
        {
            Operation = operation;
            MinorTransitions = new List<ITransition>();
        }
        public bool IsFeasible()
        {
            return true;
        }
    }

    class OperationInfo
    {
        public int i { get; set; }
        public int m { get; set; }
        public double p { get; set; }

        public OperationInfo(int i, int m, int p)
        {
            this.i = i;
            this.m = m;
            this.p = p;
        }

        public OperationInfo(string operationInfoString)
        {
            string[] split = operationInfoString.Split(", ");
            i = Int32.Parse( split[0] );
            m = OperationNameToType( split[1] );
            p = Double.Parse( split[2] );
        }

        public string Print()
        {
            return String.Format
            (
                "{0}, {1}, {2}",
                this.i,
                OperationTypeToName(this.m),
                this.p
            );
        }

        public static int OperationNameToType(string operationName)
        {
           return operationName switch
                {
                    "Move" => 1,
                    "Merge" => 2,
                    "Mix" => 3,
                    "Split" => 4,
                    "Dispense" => 5,
                    "Heat" => 6
                };
        }

        // Translates a move type integer to the according move name
        public static string OperationTypeToName(int operationType)
        {
            return operationType switch
                {
                    1 => "Move",
                    2 => "Merge",
                    3 => "Mix",
                    4 => "Split",
                    5 => "Dispense",
                    6 => "Heat"
                };
        }
    }
    class Operation
    {
        public List<DropletState> DropletStatesIn { get; }
        public OperationInfo OperationInfo { get; }
        public List<DropletState> DropletStatesOut { get; }

        public Operation(List<DropletState> dropletStatesIn, OperationInfo operationInfo, List<DropletState> dropletStatesOut)
        {
            DropletStatesIn = dropletStatesIn;
            OperationInfo = operationInfo;
            DropletStatesOut = dropletStatesOut;
        }

        public IOperationExecutionSequence ReturnEmptyOperationExecutionSequence()
        {
            return OperationInfo.m switch
                {
                    1 => new MoveOperationExecutionSequence(this),
                    2 => new MergeOperationExecutionSequence(this),
                    3 => new MixOperationExecutionSequence(this),
                    4 => new SplitOperationExecutionSequence(this),
                    5 => new DispenseOperationExecutionSequence(this),
                    6 => new HeatOperationExecutionSequence(this)
                };
        }

        public string Print()
        {
            string dropletStatesInString = "";
            foreach ( DropletState ds in DropletStatesIn )
            {
                dropletStatesInString = dropletStatesInString + ds.PrintDropletState() + "    ";
            }
            string operationInfoString = OperationInfo.Print();
            string dropletStatesOutString = "";
            foreach ( DropletState ds in DropletStatesOut )
            {
                dropletStatesOutString = dropletStatesOutString + ds.PrintDropletState() + "    ";
            }

            return dropletStatesInString + "  =>  " + operationInfoString + "  =>  " + dropletStatesOutString;
        }

    }
}