using System;

namespace DMFFramework
{
    public class DropletState
    {
        public int Id { get; set; }
        public Coordinate Coordinate { get; set; }
        public double Volume { get; set; }
        public int Colour { get; set; }
        // Minimum time the droplet should stay alive
        public int t { get; set; }
        // Maximum time the droplet should stay alive
        public int T { get; set; }

        public DropletState(int id, Coordinate coordinate, double volume, int colour, int t, int T)
        {
            Id = id;
            Coordinate = coordinate;
            Volume = volume;
            Colour = colour;
            this.t = t;
            this.T = T;
        }

        public DropletState(string dropletStateString, GridTopology gridTopology)
        {
            string[] split = dropletStateString.Split(" ");
            Id = Int32.Parse(split[0]);
            if (split[1] == "_" || split[2] == "_")
            {
                Coordinate = new Coordinate(null, null);   
            }
            else
            {
                Coordinate = gridTopology.GetCoordinate(Int32.Parse(split[1]), Int32.Parse(split[2]));
                //Coordinate = new Coordinate(Int32.Parse(split[1]), Int32.Parse(split[2]));
            }
            Volume = Double.Parse(split[3]);
            Colour = Int32.Parse(split[4]);
            t = Int32.Parse(split[5]);
            T = Int32.Parse(split[6]);
        }

        public DropletState(string dropletStateString, GridTopology gridTopology, BoardState boardStateBefore)
        {
            string[] split = dropletStateString.Split(" ");
            Id = Int32.Parse(split[0]);
            if (split[1] == "_" || split[2] == "_")
            {
                Coordinate = new Coordinate(null, null);   
            }
            else
            {
                Coordinate = gridTopology.GetCoordinate(Int32.Parse(split[1]), Int32.Parse(split[2]));
                //Coordinate = new Coordinate(Int32.Parse(split[1]), Int32.Parse(split[2]));
            }
            Volume = Double.Parse(split[3]);
            Colour = Int32.Parse(split[4]);
            t = Int32.Parse(split[5]);
            T = Int32.Parse(split[6]);
        }

        public DropletState(string dropletStateString, GridTopology gridTopology, BoardState boardStateBefore, BoardState boardStateAfter)
        {
            string[] split = dropletStateString.Split(" ");
            Id = Int32.Parse(split[0]);
            if (split[1] == "_" || split[2] == "_")
            {
                Coordinate = new Coordinate(null, null);   
            }
            else
            {
                Coordinate = gridTopology.GetCoordinate(Int32.Parse(split[1]), Int32.Parse(split[2]));
                //Coordinate = new Coordinate(Int32.Parse(split[1]), Int32.Parse(split[2]));
            }
            Volume = Double.Parse(split[3]);
            Colour = Int32.Parse(split[4]);
            t = Int32.Parse(split[5]);
            T = Int32.Parse(split[6]);
        }

        public string PrintDropletState()
        {   
            string returnValue = String.Format
            (
                "{0} {1} {2} {3} {4} {5} {6}",
                Id,
                Coordinate.X,
                Coordinate.Y,
                Volume,
                Colour,
                t,
                T
            );
            return returnValue;
        }
    }
}