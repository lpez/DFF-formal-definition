using System;
using System.Diagnostics;

namespace DMFFramework
{
    class Solution
    {
        public GridTopology GridTopology { get; set; }
        public HelperFunctions HelperFunctions { get; set; }
        public IMetric Metric { get; set; }
        public ProblemFormulation ProblemFormulation { get; set; }
        // List should be ordered from first to last
        public List<BoardState> BoardStateSequence { get; set; }
        // List should be ordered from first to last
        public List<Transition> TransitionSchedule { get; set; }
        public Dictionary<Operation, IOperationExecutionSequence> OperationsToExecutionSequences { get; set; }
        //public Dictionary<Operation, List<ITransition>> OperationsToExecutionSequences { get; set; }

        public Solution(GridTopology gridTopology, HelperFunctions helperFunctions, IMetric metric, ProblemFormulation problemFormulation, List<BoardState> boardStateSequence, List<Transition> transitionSchedule, Dictionary<Operation, IOperationExecutionSequence> operationsToExecutionSequences)
        {
            GridTopology = gridTopology;
            HelperFunctions = helperFunctions;
            Metric = metric;
            ProblemFormulation = problemFormulation;
            BoardStateSequence = boardStateSequence;
            TransitionSchedule = transitionSchedule;
            OperationsToExecutionSequences = OperationsToExecutionSequences;
        }
        
        public Solution(GridTopology gridTopology, HelperFunctions helperFunctions, IMetric metric, ProblemFormulation problemFormulation, List<BoardState> boardStateSequence, List<Transition> transitionSchedule)
        {
            GridTopology = gridTopology;
            HelperFunctions = helperFunctions;
            Metric = metric;
            ProblemFormulation = problemFormulation;
            BoardStateSequence = boardStateSequence;
            TransitionSchedule = transitionSchedule;
            OperationsToExecutionSequences = new Dictionary<Operation, IOperationExecutionSequence>();
            foreach (Operation operation in ProblemFormulation.Operations)
            {
                OperationsToExecutionSequences.Add(operation, operation.ReturnEmptyOperationExecutionSequence() );
            }
            foreach (Transition transition in TransitionSchedule)
            {
                foreach (ITransition minorTransition in transition.MinorTransitions)
                {
                    var minorTransitionInNeighbourIds = minorTransition.GetDropletStatesBefore().Select(dropletState => dropletState.Id);
                    foreach (Operation operation in ProblemFormulation.Operations)
                    {
                        bool shouldAdd = true;
                        foreach (var dropletState in operation.DropletStatesIn)
                        {
                            if (!minorTransitionInNeighbourIds.Contains(dropletState.Id) )
                            {
                                shouldAdd = false;
                            }    
                        }
                        if (shouldAdd)
                        {
                            OperationsToExecutionSequences[operation].MinorTransitions.Add(minorTransition);
                        }
                    }
                }
            }
        }

        public Solution(GridTopology gridTopology, HelperFunctions helperFunctions, IMetric metric, string operationGraphInput, string solutionName, List<String> fileNames)
        {
            GridTopology = gridTopology;
            HelperFunctions = helperFunctions;
            ProblemFormulation = new ProblemFormulation(operationGraphInput, gridTopology);
            BoardStateSequence = new List<BoardState>(fileNames.Count());
            for (int i = 0; i < fileNames.Count(); i++)
            {
                BoardStateSequence.Add
                (
                    new BoardState(gridTopology, solutionName, fileNames[i], helperFunctions)
                );
            }
            TransitionSchedule = new List<Transition>(fileNames.Count() - 1);
            for (int i = 0; i < fileNames.Count() - 1; i++)
            {
                TransitionSchedule.Append
                (
                    new Transition
                    (
                        BoardStateSequence[i],
                        BoardStateSequence[i+1],
                        gridTopology,
                        solutionName,
                        fileNames[i]
                    )
                );
            }
            OperationsToExecutionSequences = new Dictionary<Operation, IOperationExecutionSequence>();
            foreach (Operation operation in ProblemFormulation.Operations)
            {
                OperationsToExecutionSequences.Add(operation, operation.ReturnEmptyOperationExecutionSequence());
            }
            foreach (Transition transition in TransitionSchedule)
            {
                foreach (ITransition minorTransition in transition.MinorTransitions)
                {
                    var minorTransitionInNeighbourIds = minorTransition.GetDropletStatesBefore().Select(dropletState => dropletState.Id);
                    foreach (Operation operation in ProblemFormulation.Operations)
                    {
                        if (minorTransitionInNeighbourIds.Count() != operation.DropletStatesIn.Count())
                        {
                            continue;
                        }
                        bool shouldAdd = true;
                        foreach (var dropletState in operation.DropletStatesIn)
                        {
                            if (!minorTransitionInNeighbourIds.Contains(dropletState.Id) )
                            {
                                shouldAdd = false;
                            }
                            if (shouldAdd)
                            {
                                OperationsToExecutionSequences[operation].MinorTransitions.Add(minorTransition);
                            }
                        }
                    }
                }
            }
        }

        public void PrintSolutionToText(string solutionName)
        {
            MainClass.DeleteOutputDirectory(solutionName);
            MainClass.CreateOutputDirectory(solutionName);

            var integerNames = Enumerable.Range(0, BoardStateSequence.Count());
            

            for (int i = 0; i < BoardStateSequence.Count() - 1; i++)
            {
                string fileName = String.Format("{0}", i);
                var boardState = BoardStateSequence[i];
                var transition = TransitionSchedule[i];

                boardState.DropletConfiguration.WriteBoardDropletIdsToTxtFormat(solutionName, fileName, ProblemFormulation.DropletStates);
                boardState.ColourState.WriteColourStateToTxtFormat(solutionName, fileName, ProblemFormulation.Colours);
                transition.WriteTransitionToTxtFormat(solutionName, fileName);
            }
            var finalBoardState = BoardStateSequence[BoardStateSequence.Count() - 1];
            var finalFileName = String.Format("{0}", BoardStateSequence.Count()-1);
            finalBoardState.DropletConfiguration.WriteBoardDropletIdsToTxtFormat(solutionName, finalFileName, ProblemFormulation.DropletStates);
            finalBoardState.ColourState.WriteColourStateToTxtFormat(solutionName, finalFileName, ProblemFormulation.Colours);
                
        }

        // Writes CG and droplet id outputs to pictures using the
        // 'visualize.py' python script
        public void PrintSolutionToPng(string solutionName)
        {
            MainClass.CreateOutputDirectory(solutionName);
            var process = new Process();
            process.StartInfo.FileName = "python3";
            process.StartInfo.Arguments = String.Format("Visualize.py {0}", solutionName);
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;
            process.Start();
        }

        public bool IsFeasible()
        {
            if ( !BoardStateSequence[0].IsFeasibleStartingState() )
            {
                return false;
            }
            foreach (BoardState boardState in BoardStateSequence)
            {
                if ( !boardState.IsFeasible() )
                {
                    return false;
                }
            }
            foreach (Transition transition in TransitionSchedule)
            {
                if ( !transition.IsFeasible(GridTopology, HelperFunctions, Metric) )
                {
                    return false;
                }
            }
            foreach (Operation operation in ProblemFormulation.Operations)
            {
                if ( !( OperationsToExecutionSequences[operation].IsFeasible() ) )
                {
                    return false;
                }
            }
            return true;
        }
    }


    /*
    class Solution
    {
        public GridTopology GridTopology { get; set; }
        public HelperFunctions HelperFunctions { get; set; }
        public IMetric Metric { get; set; }
        public ProblemFormulation ProblemFormulation { get; set; }
        // List should be ordered from first to last
        public List<BoardState> BoardStateSequence { get; set; }
        // List should be ordered from first to last
        public List<Transition> TransitionSchedule { get; set; }
        public Dictionary<Operation, IOperationExecutionSequence>
        public Dictionary<Operation, List<ITransition>> OperationsToMinorTransitions { get; set; }

        public Solution(GridTopology gridTopology, HelperFunctions helperFunctions, IMetric metric, ProblemFormulation problemFormulation, List<BoardState> boardStateSequence, List<Transition> transitionSchedule, Dictionary<Operation, List<ITransition>> operationsToMinorTransitions)
        {
            GridTopology = gridTopology;
            HelperFunctions = helperFunctions;
            Metric = metric;
            ProblemFormulation = problemFormulation;
            BoardStateSequence = boardStateSequence;
            TransitionSchedule = transitionSchedule;
            OperationsToMinorTransitions = operationsToMinorTransitions;
        }
        
        public Solution(GridTopology gridTopology, HelperFunctions helperFunctions, IMetric metric, ProblemFormulation problemFormulation, List<BoardState> boardStateSequence, List<Transition> transitionSchedule)
        {
            GridTopology = gridTopology;
            HelperFunctions = helperFunctions;
            Metric = metric;
            ProblemFormulation = problemFormulation;
            BoardStateSequence = boardStateSequence;
            TransitionSchedule = transitionSchedule;
            OperationsToMinorTransitions = new Dictionary<Operation, List<ITransition>>();
            foreach (Operation operation in ProblemFormulation.Operations)
            {
                OperationsToMinorTransitions.Add(operation, new List<ITransition>());
            }
            foreach (Transition transition in TransitionSchedule)
            {
                foreach (ITransition minorTransition in transition.MinorTransitions)
                {
                    var minorTransitionInNeighbourIds = minorTransition.GetDropletStatesBefore().Select(dropletState => dropletState.Id);
                    foreach (Operation operation in ProblemFormulation.Operations)
                    {
                        if (minorTransitionInNeighbourIds.Count() != operation.DropletStatesIn.Count())
                        {
                            continue;
                        }
                        bool shouldAdd = true;
                        foreach (var dropletState in operation.DropletStatesIn)
                        {
                            if (!minorTransitionInNeighbourIds.Contains(dropletState.Id) )
                            {
                                shouldAdd = false;
                            }
                            if (shouldAdd)
                            {
                                OperationsToMinorTransitions[operation].Add(minorTransition);
                            }
                        }
                    }
                }
            }
        }

        public Solution(GridTopology gridTopology, HelperFunctions helperFunctions, IMetric metric, string operationGraphInput, string solutionName, List<String> fileNames)
        {
            GridTopology = gridTopology;
            HelperFunctions = helperFunctions;
            ProblemFormulation = new ProblemFormulation(operationGraphInput, gridTopology);
            BoardStateSequence = new List<BoardState>(fileNames.Count());
            for (int i = 0; i < fileNames.Count(); i++)
            {
                BoardStateSequence.Add
                (
                    new BoardState(gridTopology, solutionName, fileNames[i], helperFunctions)
                );
            }
            TransitionSchedule = new List<Transition>(fileNames.Count() - 1);
            for (int i = 0; i < fileNames.Count() - 1; i++)
            {
                TransitionSchedule.Append
                (
                    new Transition
                    (
                        BoardStateSequence[i],
                        BoardStateSequence[i+1],
                        gridTopology,
                        solutionName,
                        fileNames[i]
                    )
                );
            }
            OperationsToMinorTransitions = new Dictionary<Operation, List<ITransition>>();
            foreach (Operation operation in ProblemFormulation.Operations)
            {
                OperationsToMinorTransitions.Add(operation, new List<ITransition>());
            }
            foreach (Transition transition in TransitionSchedule)
            {
                foreach (ITransition minorTransition in transition.MinorTransitions)
                {
                    var minorTransitionInNeighbourIds = minorTransition.GetDropletStatesBefore().Select(dropletState => dropletState.Id);
                    foreach (Operation operation in ProblemFormulation.Operations)
                    {
                        if (minorTransitionInNeighbourIds.Count() != operation.DropletStatesIn.Count())
                        {
                            continue;
                        }
                        bool shouldAdd = true;
                        foreach (var dropletState in operation.DropletStatesIn)
                        {
                            if (!minorTransitionInNeighbourIds.Contains(dropletState.Id) )
                            {
                                shouldAdd = false;
                            }
                            if (shouldAdd)
                            {
                                OperationsToMinorTransitions[operation].Add(minorTransition);
                            }
                        }
                    }
                }
            }
        }

        public void PrintSolutionToText(string solutionName)
        {
            MainClass.DeleteOutputDirectory(solutionName);
            MainClass.CreateOutputDirectory(solutionName);

            var integerNames = Enumerable.Range(0, BoardStateSequence.Count());
            

            for (int i = 0; i < BoardStateSequence.Count() - 1; i++)
            {
                string fileName = String.Format("{0}", i);
                var boardState = BoardStateSequence[i];
                var transition = TransitionSchedule[i];

                boardState.DropletConfiguration.WriteBoardDropletIdsToTxtFormat(solutionName, fileName, ProblemFormulation.DropletStates);
                boardState.ColourState.WriteColourStateToTxtFormat(solutionName, fileName, ProblemFormulation.Colours);
                transition.WriteTransitionToTxtFormat(solutionName, fileName);
            }
            var finalBoardState = BoardStateSequence[BoardStateSequence.Count() - 1];
            var finalFileName = String.Format("{0}", BoardStateSequence.Count()-1);
            finalBoardState.DropletConfiguration.WriteBoardDropletIdsToTxtFormat(solutionName, finalFileName, ProblemFormulation.DropletStates);
            finalBoardState.ColourState.WriteColourStateToTxtFormat(solutionName, finalFileName, ProblemFormulation.Colours);
                
        }

        // Writes CG and droplet id outputs to pictures using the
        // 'visualize.py' python script
        public void PrintSolutionToPng(string solutionName)
        {
            MainClass.CreateOutputDirectory(solutionName);
            var process = new Process();
            process.StartInfo.FileName = "python3";
            process.StartInfo.Arguments = String.Format("Visualize.py {0}", solutionName);
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;
            process.Start();
        }

        public bool IsFeasible()
        {
            if ( !BoardStateSequence[0].IsFeasibleStartingState() )
            {
                return false;
            }
            foreach (BoardState boardState in BoardStateSequence)
            {
                if ( !boardState.IsFeasible() )
                {
                    return false;
                }
            }
            foreach (Transition transition in TransitionSchedule)
            {
                if ( !transition.IsFeasible(GridTopology, HelperFunctions, Metric) )
                {
                    return false;
                }
            }
            foreach (Operation operation in ProblemFormulation.Operations)
            {
                if ( !( operation.ReturnEmptyOperationExecutionSequence().IsFeasible() ) )
                {
                    return false;
                }
            }
            return true;
        }
    }
    */
}