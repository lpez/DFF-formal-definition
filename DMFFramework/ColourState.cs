using System;

namespace DMFFramework
{
    public class ColourState
    {
        public GridTopology GridTopology { get; set; }
        public List<List<int>> Colours { get; set; }
        public ColourState(GridTopology gridTopology, List<List<int>> colours)
        {
            GridTopology = gridTopology;
            Colours = colours;
        }

        public ColourState(GridTopology gridTopology, string solutionName, string fileName)
        {
            string filePath = "output/" + solutionName + "/txt-format/colour-states/" + fileName + ".txt";
            string[] textLines = File.ReadAllText(filePath).Split("\n", StringSplitOptions.RemoveEmptyEntries);
            textLines.Reverse();
            
            GridTopology = gridTopology;
            Colours = new List<List<int>>(gridTopology.Width);
            for (int i = 0; i < gridTopology.Width; i++)
            {
                Colours.Append(new List<int>(gridTopology.Height));
            }
            
            for (int j = 0; j < textLines.Count()-1; j++){
                string[] textCells = textLines[j].Split(" ");
                for (int i = 0; i < textCells.Count(); i++)
                {
                    Colours[i][j] = Int32.Parse(textCells[i]);
                }
            }
        }

        public int GetColour(Coordinate cell)
        {
            return Colours[cell.X][cell.Y];
        }

// Printing functions
        public void WriteColourStateToTxtFormat(string solutionName, string fileName, int[] allColours)
        {
            string filePath = "output/" + solutionName + "/txt-format/colour-states/" + fileName + ".txt";
            StreamWriter sw = File.CreateText(filePath);
            sw.AutoFlush = true;

            string allColoursString = allColours.Contains(0) ? "" : "0 ";
            foreach (int colour in allColours)
            {
                allColoursString = allColoursString + colour.ToString() + " ";
            }
            sw.WriteLine(allColoursString);

            for (int j = 0; j < GridTopology.Height; j++)
            {
                String contentLine = "";
                for (int i = 0; i < GridTopology.Width; i++)
                {
                    int cellContent = Colours[i][j];
                    string cellContentString = Colours[i][j].ToString();
                    contentLine = contentLine + cellContentString + " ";
                }
                sw.WriteLine(contentLine); 
            }
        }
    }
}