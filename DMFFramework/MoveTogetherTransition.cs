using System;

namespace DMFFramework
{
    class MoveTogetherTransition : ITransition
    {
        DropletState Droplet1BeforeMove;
        DropletState Droplet2BeforeMove;
        DropletState Droplet1AfterMove;
        DropletState Droplet2AfterMove;

        public MoveTogetherTransition(DropletState droplet1BeforeMove, DropletState droplet2BeforeMove, DropletState droplet1AfterMove, DropletState droplet2AfterMove)
        {
            Droplet1BeforeMove = droplet1BeforeMove;
            Droplet2BeforeMove = droplet2BeforeMove;
            Droplet1AfterMove = droplet1AfterMove;
            Droplet2AfterMove = droplet2AfterMove;
        }

        public List<DropletState> GetDropletStatesBefore()
        {
            return new List<DropletState>(2) { Droplet1BeforeMove, Droplet2BeforeMove };
        }

        public List<DropletState> GetDropletStatesAfter()
        {
            return new List<DropletState>(2) { Droplet1AfterMove, Droplet2AfterMove };
        }

        public string WriteTransitionToTxtFormat()
        {
            string returnString = String.Format
            (
                "2#{0} | {1} => {2} | {3}",
                Droplet1BeforeMove.PrintDropletState(),
                Droplet2BeforeMove.PrintDropletState(),
                Droplet1AfterMove.PrintDropletState(),
                Droplet2AfterMove.PrintDropletState()
            );
            return returnString;
        }

        public bool IsFeasible(GridTopology gridTopology, BoardState boardStateBefore, HelperFunctions helperFunctions, IMetric metric)
        {
            if
            (
                IdsAreCorrect()
                && VolumesStayTheSame()
                && MovesAreToAdjacentCells(gridTopology)
                && DropletsDontOverlap(gridTopology)
                && ColoursAreCorrect(gridTopology, boardStateBefore, helperFunctions)
                && NewPositionsAreDistinct()
                && NewPositionsAreAllowedToMoveToRegardsToPollution(gridTopology, boardStateBefore, helperFunctions)
            )
            {
                return true;
            }
            return false;
        }

// DIFFERENT FEASIBILITY CHECKS

        private bool IdsAreCorrect()
        {
            if 
            ( 
                Droplet1BeforeMove.Id == Droplet1AfterMove.Id
                && Droplet2BeforeMove.Id == Droplet2AfterMove.Id
                && Droplet1BeforeMove != Droplet2BeforeMove
            )
            {
                return true;
            }
            string errorMessage = 
            String.Format
            (
                "Ids are not correct for the following MoveTogetherTransition: \n {0}",
                this.WriteTransitionToTxtFormat()
            );
            Console.WriteLine(errorMessage);
            return false;
        }

        private bool VolumesStayTheSame()
        {
            if 
            ( 
                Droplet1BeforeMove.Volume == Droplet1AfterMove.Volume
                && Droplet2BeforeMove.Volume == Droplet2AfterMove.Volume
            )
            {
                return true;
            }
            string errorMessage = 
            String.Format
            (
                "Volumes are not correct for the following MoveTogetherTransition: \n {0}",
                this.WriteTransitionToTxtFormat()
            );
            Console.WriteLine(errorMessage);
            return false;
        }

        private bool MovesAreToAdjacentCells( GridTopology gridTopology )
        {
            if 
            ( 
                gridTopology.N(Droplet1BeforeMove.Coordinate, Droplet1BeforeMove.Volume).Contains(Droplet1AfterMove.Coordinate)
                && gridTopology.N(Droplet2BeforeMove.Coordinate, Droplet2BeforeMove.Volume).Contains(Droplet2AfterMove.Coordinate)
            )
            {
                return true;
            }

            Droplet1BeforeMove.Coordinate.Print();
            Console.WriteLine(" ");

            foreach (Coordinate coordinate in gridTopology.N(Droplet1BeforeMove.Coordinate, Droplet1BeforeMove.Volume))
            {
                coordinate.Print();
            }

            Console.WriteLine(" ");
            Droplet1AfterMove.Coordinate.Print();

            if (gridTopology.N(Droplet1BeforeMove.Coordinate, Droplet1BeforeMove.Volume).Contains(Droplet1AfterMove.Coordinate))
            {
                Console.WriteLine("\n HEY");
            }


            string errorMessage = 
            String.Format
            (
                "Moves are not to adjacent cells for the following MoveTogetherTransition: \n {0}",
                this.WriteTransitionToTxtFormat()
            );
            Console.WriteLine(errorMessage);
            return false;
        }

        private bool DropletsDontOverlap( GridTopology gridTopology )
        {
            if ( gridTopology.InterferenceRegionIsClear(Droplet1BeforeMove, Droplet2BeforeMove) )
            {
                return true;
            }
            string errorMessage = 
            String.Format
            (
                "Droplets overlap in the following MoveTogetherTransition: \n {0}",
                this.WriteTransitionToTxtFormat()
            );
            Console.WriteLine(errorMessage);
            return false; 
        }

        private bool ColoursAreCorrect( GridTopology gridTopology, BoardState boardStateBefore, HelperFunctions helperFunctions )
        {
            bool ColourOfDroplet1Changed = false;
            bool ColourOfDroplet2Changed = false;
            // If some cell that 'Droplet1BeforeMove' will move to is polluted
            // with anything different from its own colour or 0, then 'Droplet1AfterMove'
            // should have according colour
            foreach (Coordinate cell in gridTopology.NO(Droplet1AfterMove.Coordinate, Droplet1AfterMove.Volume))
            {
                if 
                (
                    boardStateBefore.ColourState.GetColour(cell) == Droplet2BeforeMove.Colour
                    || boardStateBefore.ColourState.GetColour(cell) == helperFunctions.GetResultingColour(Droplet1BeforeMove, Droplet2BeforeMove)
                )
                {
                    if ( !(Droplet1AfterMove.Colour == helperFunctions.GetResultingColour(Droplet1BeforeMove, Droplet2BeforeMove)) )
                    {
                        string errorMessage = 
                        String.Format
                        (
                            "Colours are not correct for the following MoveTogetherTransition: \n {0}",
                            this.WriteTransitionToTxtFormat()
                        );
                        Console.WriteLine(errorMessage);
                        return false;
                    }
                    ColourOfDroplet1Changed = true;
                }
            }
            // Otherwise every cell that 'Droplet1BeforeMove' will move to is either
            // unpolluted or polluted by its own pollution class, hence moving 'Droplet1BeforeMove'
            // should not change its colour
            if ( ColourOfDroplet1Changed )
            {
                if( Droplet1BeforeMove.Colour != Droplet1AfterMove.Colour )
                {
                    string errorMessage = 
                    String.Format
                    (
                        "Colours are not correct for the following MoveTogetherTransition: \n {0}",
                        this.WriteTransitionToTxtFormat()
                    );
                    Console.WriteLine(errorMessage);
                    return false;
                }
            }

            foreach (Coordinate cell in gridTopology.NO(Droplet2AfterMove.Coordinate, Droplet2AfterMove.Volume))
            {
                if 
                (
                    boardStateBefore.ColourState.GetColour(cell) == Droplet1BeforeMove.Colour
                    || boardStateBefore.ColourState.GetColour(cell) == helperFunctions.GetResultingColour(Droplet1BeforeMove, Droplet2BeforeMove)
                )
                {
                    if ( !(Droplet2AfterMove.Colour == helperFunctions.GetResultingColour(Droplet1BeforeMove, Droplet2BeforeMove)) )
                    {
                        string errorMessage = 
                        String.Format
                        (
                            "Colours are not correct for the following MoveTogetherTransition: \n {0}",
                            this.WriteTransitionToTxtFormat()
                        );
                        Console.WriteLine(errorMessage);
                        return false;
                    }
                    ColourOfDroplet1Changed = true;
                }
            }

            if ( ColourOfDroplet2Changed )
            {
                if ( Droplet2BeforeMove.Colour != Droplet2AfterMove.Colour )
                {
                    string errorMessage = 
                    String.Format
                    (
                        "Colours are not correct for the following MoveTogetherTransition: \n {0}",
                        this.WriteTransitionToTxtFormat()
                    );
                    Console.WriteLine(errorMessage);
                    return false;
                }
            }
            return true;
        }

        private bool NewPositionsAreDistinct()
        {
            if ( Droplet1AfterMove.Coordinate.OnTopOf( Droplet2AfterMove.Coordinate ) )
            {
                string errorMessage = 
                String.Format
                (
                    "New positions are not distinct in the following MoveTogetherTransition: \n {0}",
                    this.WriteTransitionToTxtFormat()
                );
                Console.WriteLine(errorMessage);
                return false;
            }
            return true;
        }

        private bool NewPositionsAreAllowedToMoveToRegardsToPollution( GridTopology gridTopology, BoardState boardStateBefore, HelperFunctions helperFunctions )
        {
            foreach (Coordinate cell in gridTopology.NO(Droplet1AfterMove.Coordinate, Droplet1AfterMove.Volume))
            {
                if 
                (
                    !( boardStateBefore.ColourState.GetColour(cell) == 0 )
                    && !( boardStateBefore.ColourState.GetColour(cell) == Droplet1BeforeMove.Colour )
                    && !( boardStateBefore.ColourState.GetColour(cell) == Droplet2BeforeMove.Colour )
                    && !( boardStateBefore.ColourState.GetColour(cell) == helperFunctions.GetResultingColour(Droplet1BeforeMove, Droplet2BeforeMove) )
                )
                {
                    string errorMessage = 
                    String.Format
                    (
                        "Droplet {0} moves to a polluted position in the following MoveTogetherTransition: \n {1}",
                        Droplet1AfterMove.Id,
                        this.WriteTransitionToTxtFormat()
                    );
                    Console.WriteLine(errorMessage);
                    return false;
                }
            }

            foreach (Coordinate cell in gridTopology.NO(Droplet2AfterMove.Coordinate, Droplet2AfterMove.Volume))
            {
                if 
                (
                    !( boardStateBefore.ColourState.GetColour(cell) == 0 )
                    && !( boardStateBefore.ColourState.GetColour(cell) == Droplet1BeforeMove.Colour )
                    && !( boardStateBefore.ColourState.GetColour(cell) == Droplet2BeforeMove.Colour )
                    && !( boardStateBefore.ColourState.GetColour(cell) == helperFunctions.GetResultingColour(Droplet1BeforeMove, Droplet2BeforeMove) )
                )
                {
                    string errorMessage = 
                    String.Format
                    (
                        "Droplet {0} moves to a polluted position in the following MoveTogetherTransition: \n {1}",
                        Droplet2AfterMove.Id,
                        this.WriteTransitionToTxtFormat()
                    );
                    Console.WriteLine(errorMessage);
                    return false;
                }
            }
            return true;
        }

    }

}