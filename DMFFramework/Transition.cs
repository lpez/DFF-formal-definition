using System;

namespace DMFFramework
{
    // Interface should be implemented by all types of action transitions
    public interface ITransition
    {
        public bool IsFeasible( GridTopology gridTopology, BoardState boardStateBefore, HelperFunctions helperFunctions, IMetric metric );
        public List<DropletState> GetDropletStatesBefore();
        public List<DropletState> GetDropletStatesAfter();
        public string WriteTransitionToTxtFormat();
    }


    class Transition
    {
        public BoardState BoardStateBefore { get; set; }
        public BoardState BoardStateAfter { get; set; }
        public List<ITransition> MinorTransitions { get; set; }
        public Transition(BoardState boardStateBefore, BoardState boardStateAfter, List<ITransition> minorTransitions)
        {
            BoardStateBefore = boardStateBefore;
            BoardStateAfter = boardStateAfter;
            MinorTransitions = minorTransitions;
        }

        public Transition(BoardState boardStateBefore, List<ITransition> minorTransitions)
        {
            BoardStateBefore = boardStateBefore;
            BoardStateAfter = boardStateBefore.GetNextBoardstate(MinorTransitions);
            MinorTransitions = minorTransitions;
        }


        public Transition(BoardState boardStateBefore, BoardState boardStateAfter, GridTopology gridTopology, string solutionName, string fileName)
        {
            BoardStateBefore = boardStateBefore;
            BoardStateAfter = boardStateAfter;
            MinorTransitions = new List<ITransition>();
            
            string filePath = "output/" + solutionName + "/txt-format/transitions/" + fileName + ".txt";
            var readFile = File.ReadAllText(filePath);
            string[] minorTransitions = readFile.Split("\n", StringSplitOptions.RemoveEmptyEntries);
            foreach (string minorTransitionString in minorTransitions)
            {
                string[] typeAndMinorTransitionString = minorTransitionString.Split("#");
                string typeString = typeAndMinorTransitionString[0];
                string[] subsets = typeAndMinorTransitionString[1].Split("=>", StringSplitOptions.RemoveEmptyEntries);
                string[] dropletStateStringsFrom = subsets[0].Split(" | ");
                string[] dropletStateStringsTo = subsets[1].Split(" | ");

                ITransition minorTransition;

                if (typeString == "1")
                {
                    MinorTransitions.Add
                    (
                        new MoveTransition
                        (
                            new DropletState(dropletStateStringsFrom[0], gridTopology),
                            new DropletState(dropletStateStringsTo[0], gridTopology)
                        )
                    );
                } 
                else if (typeString == "2")
                {
                    MinorTransitions.Add
                    (
                        new MoveTogetherTransition
                        (
                            new DropletState(dropletStateStringsFrom[0], gridTopology),
                            new DropletState(dropletStateStringsFrom[1], gridTopology),
                            new DropletState(dropletStateStringsTo[0], gridTopology),
                            new DropletState(dropletStateStringsTo[1], gridTopology)
                        )
                    );
                }        
                else if (typeString == "3")
                {
                    MinorTransitions.Add
                    (
                        new MergeInProgressTransition
                        (
                            new DropletState(dropletStateStringsFrom[0], gridTopology),
                            new DropletState(dropletStateStringsFrom[1], gridTopology),
                            new DropletState(dropletStateStringsTo[0], gridTopology),
                            new DropletState(dropletStateStringsTo[1], gridTopology)
                        )
                    );
                }   
                else if (typeString == "4")
                {
                    MinorTransitions.Add
                    (
                        new MergeTransition
                        (
                            new DropletState(dropletStateStringsFrom[0], gridTopology),
                            new DropletState(dropletStateStringsFrom[1], gridTopology),
                            new DropletState(dropletStateStringsTo[0], gridTopology)
                        )
                    );
                }   
                else if (typeString == "5")
                {
                    MinorTransitions.Add
                    (
                        new SplitInProgressTransition
                        (
                            new DropletState(dropletStateStringsFrom[0], gridTopology),
                            new DropletState(dropletStateStringsFrom[1], gridTopology),
                            new DropletState(dropletStateStringsTo[0], gridTopology),
                            new DropletState(dropletStateStringsTo[1], gridTopology)
                        )
                    );
                }   
                else if (typeString == "6")
                {
                    MinorTransitions.Add
                    (
                        new SplitTransition
                        (
                            new DropletState(dropletStateStringsFrom[0], gridTopology),
                            new DropletState(dropletStateStringsTo[0], gridTopology),
                            new DropletState(dropletStateStringsTo[1], gridTopology)
                        )
                    );
                }   
            }
        }

        public Transition(BoardState boardStateBefore, GridTopology gridTopology, string solutionName, string fileName)
        {
            BoardStateBefore = boardStateBefore;
            MinorTransitions = new List<ITransition>();
            
            string filePath = "output/" + solutionName + "/txt-format/transitions/" + fileName + ".txt";
            var readFile = File.ReadAllText(filePath);
            string[] minorTransitions = readFile.Split("\n", StringSplitOptions.RemoveEmptyEntries);
            foreach (string minorTransitionString in minorTransitions)
            {
                string[] typeAndMinorTransitionString = minorTransitionString.Split("#");
                string typeString = typeAndMinorTransitionString[0];
                string[] subsets = typeAndMinorTransitionString[1].Split(" => ", StringSplitOptions.RemoveEmptyEntries);
                string[] dropletStateStringsFrom = subsets[0].Split(" | ");
                string[] dropletStateStringsTo = subsets[1].Split(" | ");
                
                if (typeString == "1")
                {
                    MinorTransitions.Add
                    (
                        new MoveTransition
                        (
                            new DropletState(dropletStateStringsFrom[0], gridTopology),
                            new DropletState(dropletStateStringsTo[0], gridTopology)
                        )
                    );
                } 
                else if (typeString == "2")
                {
                    
                    MinorTransitions.Add
                    (
                        new MoveTogetherTransition
                        (
                            new DropletState(dropletStateStringsFrom[0], gridTopology),
                            new DropletState(dropletStateStringsFrom[1], gridTopology),
                            new DropletState(dropletStateStringsTo[0], gridTopology),
                            new DropletState(dropletStateStringsTo[1], gridTopology)
                        )
                    );
                }        
                else if (typeString == "3")
                {
                    MinorTransitions.Add
                    (
                        new MergeInProgressTransition
                        (
                            new DropletState(dropletStateStringsFrom[0], gridTopology),
                            new DropletState(dropletStateStringsFrom[1], gridTopology),
                            new DropletState(dropletStateStringsTo[0], gridTopology),
                            new DropletState(dropletStateStringsTo[1], gridTopology)
                        )
                    );
                }   
                else if (typeString == "4")
                {
                    MinorTransitions.Add
                    (
                        new MergeTransition
                        (
                            new DropletState(dropletStateStringsFrom[0], gridTopology),
                            new DropletState(dropletStateStringsFrom[1], gridTopology),
                            new DropletState(dropletStateStringsTo[0], gridTopology)
                        )
                    );
                }   
                else if (typeString == "5")
                {
                    MinorTransitions.Add
                    (
                        new SplitInProgressTransition
                        (
                            new DropletState(dropletStateStringsFrom[0], gridTopology),
                            new DropletState(dropletStateStringsFrom[1], gridTopology),
                            new DropletState(dropletStateStringsTo[0], gridTopology),
                            new DropletState(dropletStateStringsTo[1], gridTopology)
                        )
                    );
                }   
                else if (typeString == "6")
                {
                    MinorTransitions.Add
                    (
                        new SplitTransition
                        (
                            new DropletState(dropletStateStringsFrom[0], gridTopology),
                            new DropletState(dropletStateStringsTo[0], gridTopology),
                            new DropletState(dropletStateStringsTo[1], gridTopology)
                        )
                    );
                }   
            }
            BoardStateAfter = BoardStateBefore.GetNextBoardstate(MinorTransitions);
        }

        public void WriteTransitionToTxtFormat(string solutionName, string fileName)
        {
            string filePath = "output/" + solutionName + "/txt-format/transitions/" + fileName + ".txt";
            StreamWriter sw = File.CreateText(filePath);
            sw.AutoFlush = true;

            foreach (ITransition minorTransition in MinorTransitions)
            {
                sw.WriteLine(minorTransition.WriteTransitionToTxtFormat() + "\n");
            }
        }

        public bool IsFeasible(GridTopology gridTopology, HelperFunctions helperFunctions, IMetric metric)
        {
            foreach (ITransition minorTransition in MinorTransitions)
            {
                if ( !minorTransition.IsFeasible(gridTopology, BoardStateBefore, helperFunctions, metric) )
                {
                    return false;
                }
            }
            if 
            ( 
                !ColoursChangeCorrectly(gridTopology)
                || !TransitionIsBijective()
            )
            {
                return false;
            }
            return true;
        }

        public bool ColoursChangeCorrectly(GridTopology gridTopology)
        {
            var CellCanHaveDifferentColour = new Dictionary<Coordinate, bool>();
            // First categorize all cells that will be moved to
            foreach (DropletState ds in BoardStateAfter.DropletConfiguration.DropletStates)
            {
                foreach (Coordinate cell in gridTopology.NO(ds.Coordinate, ds.Volume))
                {
                    CellCanHaveDifferentColour.TryAdd(cell, true);
                }
            }

            // Next go through each cell in the grid and make sure the colours
            // stay constant if no new droplet moves to this cell
            Coordinate cellVariable;
            bool returnVariable;
            for (int i = 0; i < 20; i++)
            {
                for (int j = 0; j < 32; j++)
                {
                    cellVariable = gridTopology.GetCoordinate(i, j);
                    if (CellCanHaveDifferentColour.TryGetValue(cellVariable, out returnVariable))
                    {
                        continue;
                    }

                    if (BoardStateBefore.ColourState.Colours[i][j] != BoardStateAfter.ColourState.Colours[i][j])
                    {
                        string errorMessage = 
                        String.Format
                        (
                            "Colours change in cell ({0}, {1}) where no new droplets move to in the following transition:",
                            i, j
                        );
                        Console.WriteLine(errorMessage);
                        this.Print();
                        return false;
                    }
                }
            }
            return true;
        }

        private bool TransitionIsBijective()
        {
            var DropletStateIdsBefore = new Dictionary<int, bool>();
            var DropletStateIdsAfter = new Dictionary<int, bool>();
            bool DropletStateWasAdded;
            // Check that every DropletState is in at most one minor transition
            // and populate the dictionaries keeping track of which droplets are part
            // of some minor transition


            foreach (ITransition minorTransition in MinorTransitions)
            {
                foreach (DropletState ds in minorTransition.GetDropletStatesBefore())
                {
                    DropletStateWasAdded = DropletStateIdsBefore.TryAdd(ds.Id, true);

                    // If the DropletState could not be added, then this is because
                    // it was already present, hence is in more than one transition
                    if ( !DropletStateWasAdded )
                    {
                        string errorMessage = 
                        String.Format
                        (
                            "The following droplet is in more than one minor transition: \n {0} \n in the following transition",
                            ds.PrintDropletState()
                        );
                        Console.WriteLine(errorMessage);
                        this.Print();
                        return false;
                    }
                }
                foreach (DropletState ds in minorTransition.GetDropletStatesAfter())
                {
                    DropletStateWasAdded = DropletStateIdsAfter.TryAdd(ds.Id, true);
                    // If the DropletState could not be added, then this is because
                    // it was already present, hence is in more than one transition
                    if ( !DropletStateWasAdded )
                    {
                        string errorMessage = 
                        String.Format
                        (
                            "The following droplet is in more than one minor transition: \n {0} \n in the following transition",
                            ds.PrintDropletState()
                        );
                        Console.WriteLine(errorMessage);
                        this.Print();
                        return false;
                    }
                }
            }
            bool resultVariable;
            // Check that every DropletState is in at least one minor transition
            foreach (DropletState ds in BoardStateBefore.DropletConfiguration.DropletStates)
            {
                // If ds is not a key this means it was in no minor transition, 
                // hence should return 'false'
                if ( !DropletStateIdsBefore.TryGetValue(ds.Id, out resultVariable) )
                {
                    string errorMessage = 
                    String.Format
                    (
                        "The following droplet is not in any minor transition: \n {0} \n in the following transition",
                        ds.PrintDropletState()
                    );
                    Console.WriteLine(errorMessage);
                    this.Print();
                    return false;
                }
            }
            foreach (DropletState ds in BoardStateAfter.DropletConfiguration.DropletStates)
            {
                if ( !DropletStateIdsAfter.TryGetValue(ds.Id, out resultVariable) )
                {
                    string errorMessage = 
                    String.Format
                    (
                        "The following droplet is not in any minor transition: \n {0} \n in the following transition",
                        ds.PrintDropletState()
                    );
                    Console.WriteLine(errorMessage);
                    this.Print();
                    return false;
                }
            }
            return true;
        }

        private void Print()
        {   
            foreach (ITransition minorTransition in MinorTransitions)
            {
                Console.WriteLine("{0}", minorTransition.WriteTransitionToTxtFormat());
            }
        }
    }
}