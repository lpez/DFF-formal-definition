using System;

namespace DMFFramework
{
    class SplitInProgressTransition : ITransition
    {
        DropletState Droplet1BeforeMove;
        DropletState Droplet2BeforeMove;
        DropletState Droplet1AfterMove;
        DropletState Droplet2AfterMove;

        public SplitInProgressTransition(DropletState droplet1BeforeMove, DropletState droplet2BeforeMove, DropletState droplet1AfterMove, DropletState droplet2AfterMove)
        {
            Droplet1BeforeMove = droplet1BeforeMove;
            Droplet2BeforeMove = droplet2BeforeMove;
            Droplet1AfterMove = droplet1AfterMove;
            Droplet2AfterMove = droplet2AfterMove;
        }

        public List<DropletState> GetDropletStatesBefore()
        {
            return new List<DropletState>(2) { Droplet1BeforeMove, Droplet2BeforeMove };
        }

        public List<DropletState> GetDropletStatesAfter()
        {
            return new List<DropletState>(2) { Droplet1AfterMove, Droplet2AfterMove };
        }

        public string WriteTransitionToTxtFormat()
        {
            string returnString = String.Format
            (
                "5#{0} | {1} => {2} | {3}",
                Droplet1BeforeMove.PrintDropletState(),
                Droplet2BeforeMove.PrintDropletState(),
                Droplet1AfterMove.PrintDropletState(),
                Droplet2AfterMove.PrintDropletState()
            );
            return returnString;
        }

        public bool IsFeasible( GridTopology gridTopology, BoardState boardStateBefore, HelperFunctions helperFunctions, IMetric metric )
        {
            if
            (
                IdsAreCorrect()
                && VolumesAreCorrect()
                && MovesAreToAdjacentCells(gridTopology)
                && InitialDropletsOverlap(gridTopology)
                && DropletsMoveFartherAway(metric)
                && ColoursAreCorrect(helperFunctions)
                && NewPositionsAreDistinct()
                && NewPositionsAreAllowedToMoveToRegardsToPollution(gridTopology, boardStateBefore, helperFunctions)
            )
            {
                return true;
            }
            return false;   
        }

        private bool IdsAreCorrect()
        {
            if 
            ( 
                Droplet1BeforeMove.Id == Droplet1AfterMove.Id
                && Droplet2BeforeMove.Id == Droplet2AfterMove.Id
                && Droplet1BeforeMove != Droplet2BeforeMove
            )
            {
                return true;
            }
            string errorMessage = 
            String.Format
            (
                "Ids are not correct for the following SplitInProgressTransition: \n {0}",
                this.WriteTransitionToTxtFormat()
            );
            Console.WriteLine(errorMessage);
            return false;
        }

        private bool VolumesAreCorrect()
        {
            if 
            ( 
                Droplet1BeforeMove.Volume == Droplet1AfterMove.Volume
                && Droplet2BeforeMove.Volume == Droplet2AfterMove.Volume
            )
            {
                return true;
            }
            string errorMessage = 
            String.Format
            (
                "Volumes are not correct for the following SplitInProgressTransition: \n {0}",
                this.WriteTransitionToTxtFormat()
            );
            Console.WriteLine(errorMessage);
            return false;
        }

        private bool MovesAreToAdjacentCells( GridTopology gridTopology )
        {
            if 
            ( 
                gridTopology.N(Droplet1BeforeMove.Coordinate, Droplet1BeforeMove.Volume).Contains(Droplet1AfterMove.Coordinate)
                && gridTopology.N(Droplet2BeforeMove.Coordinate, Droplet2BeforeMove.Volume).Contains(Droplet2AfterMove.Coordinate)
            )
            {
                return true;
            }
            string errorMessage = 
            String.Format
            (
                "Moves are not to adjacent cells in the following SplitInProgressTransition: \n {0}",
                this.WriteTransitionToTxtFormat()
            );
            Console.WriteLine(errorMessage);
            return false;
        }

        private bool InitialDropletsOverlap( GridTopology gridTopology )
        {
            if ( gridTopology.InterferenceRegionIsClear(Droplet1BeforeMove, Droplet2BeforeMove))
            {
               string errorMessage = 
                String.Format
                (
                    "Initial droplets overlap in the following SplitInProgressTransition: \n {0}",
                    this.WriteTransitionToTxtFormat()
                );
                Console.WriteLine(errorMessage);
                return false;
            }
            return true;
        }

        private bool DropletsMoveFartherAway( IMetric metric )
        {
            if ( metric.Distance(Droplet1AfterMove, Droplet2AfterMove) > metric.Distance(Droplet1BeforeMove, Droplet2BeforeMove) )
            {
                return true;
            }
            string errorMessage = 
            String.Format
            (
                "Droplets do not move apart in the following SplitInProgressTransition: \n {0}",
                this.WriteTransitionToTxtFormat()
            );
            Console.WriteLine(errorMessage);
            return false;
        }

        private bool ColoursAreCorrect( HelperFunctions helperFunctions )
        {
            if 
            (
                Droplet1BeforeMove.Colour == Droplet2BeforeMove.Colour 
                && Droplet2BeforeMove.Colour == Droplet1AfterMove.Colour 
                && Droplet1AfterMove.Colour == Droplet2AfterMove.Colour 
                && Droplet2AfterMove.Colour == helperFunctions.GetResultingColour(Droplet1BeforeMove, Droplet2BeforeMove)
            )
            {
                return true;
            }
            string errorMessage = 
            String.Format
            (
                "Colours are not correct in the following SplitInProgressTransition: \n {0}",
                this.WriteTransitionToTxtFormat()
            );
            Console.WriteLine(errorMessage);
            return false;
        }

        private bool NewPositionsAreDistinct()
        {
            if ( Droplet1AfterMove.Coordinate.OnTopOf( Droplet2AfterMove.Coordinate ) )
            {
                string errorMessage = 
                String.Format
                (
                    "New positions are not distinct in the following SplitInProgressTransition: \n {0}",
                    this.WriteTransitionToTxtFormat()
                );
                Console.WriteLine(errorMessage);
                return false;
            }
            return true;
        }

        private bool NewPositionsAreAllowedToMoveToRegardsToPollution( GridTopology gridTopology, BoardState boardStateBefore, HelperFunctions helperFunctions )
        {
            foreach (Coordinate cell in gridTopology.NO(Droplet1AfterMove.Coordinate, Droplet1AfterMove.Volume))
            {
                if 
                (
                    !( boardStateBefore.ColourState.GetColour(cell) == 0 )
                    && !( boardStateBefore.ColourState.GetColour(cell) == helperFunctions.GetResultingColour(Droplet1BeforeMove, Droplet2BeforeMove) )
                )
                {
                    string errorMessage = 
                    String.Format
                    (
                        "The position of droplet {0} is polluted in the following SplitInProgressTransition: \n {1}",
                        Droplet1AfterMove.Id,
                        this.WriteTransitionToTxtFormat()
                    );
                    Console.WriteLine(errorMessage);
                    return false;
                }
            }

            foreach (Coordinate cell in gridTopology.NO(Droplet2AfterMove.Coordinate, Droplet2AfterMove.Volume))
            {
                if 
                (
                    !( boardStateBefore.ColourState.GetColour(cell) == 0 )
                    && !( boardStateBefore.ColourState.GetColour(cell) == helperFunctions.GetResultingColour(Droplet1BeforeMove, Droplet2BeforeMove) )
                )
                {
                    string errorMessage = 
                    String.Format
                    (
                        "The position of droplet {0} is polluted in the following SplitInProgressTransition: \n {1}",
                        Droplet2AfterMove.Id,
                        this.WriteTransitionToTxtFormat()
                    );
                    Console.WriteLine(errorMessage);
                    return false;
                }
            }
            return true;
        }

    }
}