using System;

namespace DMFFramework
{
    public interface IMetric
    {
        public int Distance(DropletState ds1, DropletState ds2);
    }

    public class ManhattanMetric : IMetric
    {
        public int Distance(DropletState ds1, DropletState ds2)
        {
            return Math.Abs(ds1.Coordinate.X - ds2.Coordinate.X) + Math.Abs(ds1.Coordinate.Y - ds2.Coordinate.Y);
        }
    }
}