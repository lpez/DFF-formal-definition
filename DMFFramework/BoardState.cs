using System;

namespace DMFFramework
{
    public class BoardState
    {
        public GridTopology GridTopology { get; set; }
        public ColourState ColourState { get; set; }
        public DropletConfiguration DropletConfiguration { get; set; }
        public HelperFunctions HelperFunctions { get; set; }
        public BoardState(GridTopology gridTopology, ColourState colourState, DropletConfiguration dropletConfiguration, HelperFunctions helperFunctions)
        {
            GridTopology = gridTopology;
            ColourState = colourState;
            DropletConfiguration = dropletConfiguration;
            HelperFunctions = helperFunctions;
        }

        public BoardState(GridTopology gridTopology, string solutionName, string fileName, HelperFunctions helperFunctions)
        {
            GridTopology = gridTopology;
            ColourState = new ColourState(gridTopology, solutionName, fileName);
            DropletConfiguration = new DropletConfiguration(gridTopology, solutionName, fileName);
            HelperFunctions = helperFunctions;
        }

        public BoardState GetNextBoardstate(List<ITransition> minorTransitions)
        {
            // Copy the current ColourState so we can later populate it
            var nextColours = new List<List<int>>(GridTopology.Width);
            for (int i = 0; i < GridTopology.Width; i++)
            {
                nextColours.Add(new List<int>(GridTopology.Height));
                for (int j = 0; j < GridTopology.Height; j++)
                {
                    nextColours[i].Add(ColourState.Colours[i][j]);
                }
            }
            
            // Initialize nextDropletConfiguration and nextColourState
            var nextDropletStates =  new List<DropletState>();
            foreach (ITransition minorTransition in minorTransitions)
            {
                foreach (DropletState ds in minorTransition.GetDropletStatesAfter())
                {
                    foreach (Coordinate cell in GridTopology.NO(ds.Coordinate, ds.Volume))
                    {
                        nextColours[cell.X][cell.Y] = ds.Colour;
                    }
                    nextDropletStates.Add(ds);
                }
            }
            ColourState nextColourState = new ColourState(GridTopology, nextColours);
            DropletConfiguration nextDropletConfiguration = new DropletConfiguration(GridTopology, nextDropletStates);

            return new BoardState(GridTopology, nextColourState, nextDropletConfiguration, HelperFunctions);
        }

// FEASIBILITY CHECKS

        

        public bool IsFeasible()
        {
            if (OccupiedRegionsArePollutedCorrectly())
            {
                return true;
            }
            return false;
        }

        public bool IsFeasibleStartingState()
        {
            if
            (
                OccupiedRegionsArePollutedCorrectly()
                && NoDropletsOverlapInitially()
                && UnoccupiedRegionsAreInitiallyUnpolluted()
            )
            {
                return true;
            }
            return false;
        }

        private bool OccupiedRegionsArePollutedCorrectly()
        {
            foreach (DropletState ds in DropletConfiguration.DropletStates)
            {
                foreach (Coordinate cell in GridTopology.NO(ds.Coordinate, ds.Volume))
                {
                    if (ColourState.GetColour(cell) != ds.Colour)
                    {
                        string errorMessage = 
                        String.Format
                        (
                            "Cell ({0}, {1}) has colour {2} but should instead have colour {3}",
                            cell.X,
                            cell.Y,
                            ColourState.GetColour(cell),
                            ds.Colour
                        );
                        Console.WriteLine(errorMessage);
                        return false;
                    }
                }
            }
            return true;
        }

        private bool UnoccupiedRegionsAreInitiallyUnpolluted()
        {
            var CellIsOccupied = new Dictionary<Coordinate, bool>();
            foreach (DropletState ds in DropletConfiguration.DropletStates)
            {
                foreach (Coordinate cell in GridTopology.NO(ds.Coordinate, ds.Volume))
                {
                    CellIsOccupied.TryAdd(cell, true);
                }
            }

            bool returnVariable;
            Coordinate cellVariable;
            for (int i = 0; i < 20; i++)
            {
                for (int j = 0; j < 32; j++)
                {
                    cellVariable = GridTopology.GetCoordinate(i, j);
                    if( !CellIsOccupied.TryGetValue( cellVariable, out returnVariable ) )
                    {
                        if ( ColourState.GetColour(cellVariable) != 0 )
                        {
                            string errorMessage = 
                            String.Format
                            (
                                "Starting cell is initially polluted:  ({0}, {1})",
                                i,
                                j
                            );
                            Console.WriteLine(errorMessage);
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        private bool NoDropletsOverlapInitially()
        {
            foreach (DropletState ds1 in DropletConfiguration.DropletStates)
            {
                foreach (DropletState ds2 in DropletConfiguration.DropletStates)
                {
                    if (ds1 != ds2)
                    {
                        if ( !GridTopology.InterferenceRegionIsClear(ds1, ds2) )
                        {
                            string errorMessage = 
                            String.Format
                            (
                                "Overlapping droplets: \n {0} \n {1}",
                                ds1.PrintDropletState(),
                                ds2.PrintDropletState()
                            );
                            Console.WriteLine(errorMessage);
                            return false;
                        }
                    }
                }
            }
            return true;
        }
    }
}