using System;
using System.IO;

namespace DMFFramework
{
    public class HelperFunctions
    {
        public string GraphInput { get; set; }
        public string MixingRules { get; set; }
        public List<List<int>> ColourMixingFunction { get; set; }
        public List<List<bool>> QuarantineFunction { get; set; }

        public HelperFunctions(string graphInput, string mixingRules)
        {
            GraphInput = File.ReadAllText(graphInput);
            MixingRules = File.ReadAllText(mixingRules);
            InitializeColourMixingFunction();
        }

// GETTERS
        public int GetResultingColour(DropletState ds1, DropletState ds2)
        {
            return ColourMixingFunction[ds1.Colour][ds2.Colour];
        }

        public bool CanCrossPaths(DropletState ds1, DropletState ds2)
        {
            return QuarantineFunction[ds1.Id][ds2.Id];
        }


// INITIALIZER FUNCTIONS
        private void InitializeColourMixingFunction()
        {
            string[] allCcs = MixingRules.Split("|\n")[0].Split("\n", StringSplitOptions.RemoveEmptyEntries);
            string[] ccRules = MixingRules.Split("|\n")[1].Split("\n", StringSplitOptions.RemoveEmptyEntries);

            // Sets E[c1][c2] = 0 for all c1 and c2 not equal to 0,
            // sets E[c1][c1] = c1
            // Sets E[0][c1] = c1
            ColourMixingFunction = new List<List<int>>(allCcs.Count());
            for (int i = 0; i < allCcs.Count(); i++)
            {
                ColourMixingFunction.Add
                (
                    Enumerable.Range(0, allCcs.Count()).Select(x => 0).ToList()
                );
                ColourMixingFunction[i][i] = i;
                ColourMixingFunction[0][i] = i;
                ColourMixingFunction[i][0] = i;
            }
            // Sets the colour classes when they're present in the input file
            foreach (string ccRule in ccRules)
            {
                string[] ccs = ccRule.Split(", ");
                int cc1 = Int32.Parse(ccs[0]);
                int cc2 = Int32.Parse(ccs[1]);
                int ccResult = Int32.Parse(ccs[2]);

                ColourMixingFunction[cc1][cc2] = ccResult;
                ColourMixingFunction[cc2][cc1] = ccResult;
            }   
        }        
    }
}