using System;
using System.Diagnostics;

namespace DMFFramework 
{
    public class MainClass
    {
        public static void Main(string[] args)
        {
// EXAMPLE 1
            /*
            string operationGraphInput = "operation-graphs/graph-merge.txt";
            string mixingRulesInput = "mixing-rules/ruleset1.txt";
            GridTopology gridTopology = new GridTopology(20, 32, new List<(Coordinate, int)>());
            HelperFunctions helperFunctions = new HelperFunctions(operationGraphInput, mixingRulesInput);
            ManhattanMetric manhattanMetric = new ManhattanMetric();

            ProblemFormulation problemFormulation = new ProblemFormulation(operationGraphInput);
            BoardState startingState = problemFormulation.GetStartingState(gridTopology, helperFunctions);

            //CreateOutputDirectory("solution1");
            //startingState.ColourState.WriteColourStateToTxtFormat( "solution1", "1", new int[]{ 0, 1, 2, 10 } );
            //startingState.DropletConfiguration.WriteBoardDropletIdsToTxtFormat("solution1", "1", problemFormulation.DropletStates);

            BoardState currentBoardState = startingState;
            Transition currentTransition;
            var boardStateSequence = new List<BoardState>() {currentBoardState};
            var transitionSchedule = new List<Transition>();

            foreach (string fileName in new string[] {"1", "2", "3", "4", "5", "6"} )
            {
                currentTransition = new Transition(currentBoardState, "solution1", fileName);
                currentBoardState = currentTransition.BoardStateAfter;
                boardStateSequence.Add(currentBoardState);
                transitionSchedule.Add(currentTransition);
            }

            var solution = new Solution(gridTopology, helperFunctions, manhattanMetric, problemFormulation, boardStateSequence, transitionSchedule);
            solution.PrintSolutionToText("solution2");
            solution.PrintSolutionToPng("solution2");

            Console.WriteLine("is feasible: {0}", solution.IsFeasible());

            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            stopwatch.Stop();
            Console.WriteLine("Elapsed time is {0} ms", stopwatch.ElapsedMilliseconds);
            */
        
// SEMINAR EXAMPLE
            string operationGraphInput = "operation-graphs/seminar-example.txt";
            string mixingRulesInput = "mixing-rules/ruleset1.txt";
            GridTopology gridTopology = new GridTopology(20, 32, new List<(Coordinate, int)>());
            ProblemFormulation problemFormulation = new ProblemFormulation(operationGraphInput, gridTopology);
            HelperFunctions helperFunctions = new HelperFunctions(operationGraphInput, mixingRulesInput);
            ManhattanMetric manhattanMetric = new ManhattanMetric();

            BoardState startingState = problemFormulation.GetStartingState(gridTopology, helperFunctions);

            BoardState currentBoardState = startingState;
            Transition currentTransition;
            var boardStateSequence = new List<BoardState>() {currentBoardState};
            var transitionSchedule = new List<Transition>();

            foreach (string fileName in new string[] {"0", "1", "2"} )
            {
                currentTransition = new Transition(currentBoardState, gridTopology, "seminar-example", fileName);
                currentBoardState = currentTransition.BoardStateAfter;
                boardStateSequence.Add(currentBoardState);
                transitionSchedule.Add(currentTransition);
            }

            var solution = new Solution(gridTopology, helperFunctions, manhattanMetric, problemFormulation, boardStateSequence, transitionSchedule);
            Console.WriteLine("is feasible: {0}", solution.IsFeasible());
            solution.PrintSolutionToText("seminar-example");
            solution.PrintSolutionToPng("seminar-example");
        }


        public static void CreateInputDirectory(string solutionName)
        {
            string filePath = "input/" + solutionName;
            if ( !Directory.Exists(filePath) )
            {
                Directory.CreateDirectory(filePath);

                Directory.CreateDirectory(filePath + "/jpg-format");
                Directory.CreateDirectory(filePath + "/jpg-format/colour-states");
                Directory.CreateDirectory(filePath + "/jpg-format/droplet-configurations");

                Directory.CreateDirectory(filePath + "/txt-format");
                Directory.CreateDirectory(filePath + "/txt-format/colour-states");
                Directory.CreateDirectory(filePath + "/txt-format/droplet-configurations");
                Directory.CreateDirectory(filePath + "/txt-format/transitions");
            }
        }

        public static void DeleteInputDirectory(string solutionName)
        {
            string filePath = "input/" + solutionName;
            if ( Directory.Exists(filePath) )
            {
                Directory.Delete(filePath, true);
            }
        }

        public static void CreateOutputDirectory(string solutionName)
        {
            string filePath = "output/" + solutionName;
            if ( !Directory.Exists(filePath) )
            {
                Directory.CreateDirectory(filePath);

                Directory.CreateDirectory(filePath + "/jpg-format");
                Directory.CreateDirectory(filePath + "/jpg-format/colour-states");
                Directory.CreateDirectory(filePath + "/jpg-format/droplet-configurations");
                Directory.CreateDirectory(filePath + "/jpg-format/droplets-and-colours");

                Directory.CreateDirectory(filePath + "/txt-format");
                Directory.CreateDirectory(filePath + "/txt-format/colour-states");
                Directory.CreateDirectory(filePath + "/txt-format/droplet-configurations");
                Directory.CreateDirectory(filePath + "/txt-format/transitions");
            }
        }

        public static void DeleteOutputDirectory(string solutionName)
        {
            string filePath = "output/" + solutionName;
            if ( Directory.Exists(filePath) )
            {
                Directory.Delete(filePath, true);
            }
        }
    }
}