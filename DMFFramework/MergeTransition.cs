using System;

namespace DMFFramework
{
    class MergeTransition : ITransition
    {
        DropletState Droplet1BeforeMove;
        DropletState Droplet2BeforeMove;
        DropletState DropletAfterMove;

        public MergeTransition(DropletState droplet1BeforeMove, DropletState droplet2BeforeMove, DropletState dropletAfterMove)
        {
            Droplet1BeforeMove = droplet1BeforeMove;
            Droplet2BeforeMove = droplet2BeforeMove;
            DropletAfterMove = dropletAfterMove;
        }

        public List<DropletState> GetDropletStatesBefore()
        {
            return new List<DropletState>(2) { Droplet1BeforeMove, Droplet2BeforeMove };
        }

        public List<DropletState> GetDropletStatesAfter()
        {
            return new List<DropletState>(1) { DropletAfterMove };
        }

        public string WriteTransitionToTxtFormat()
        {
            string returnString = String.Format
            (
                "4#{0} | {1} => {2} | _",
                Droplet1BeforeMove.PrintDropletState(),
                Droplet2BeforeMove.PrintDropletState(),
                DropletAfterMove.PrintDropletState()
            );
            return returnString;
        }

        public bool IsFeasible( GridTopology gridTopology, BoardState boardStateBefore, HelperFunctions helperFunctions, IMetric metric )
        {
            if
            (
                IdsAreCorrect()
                && VolumesAreCorrect()
                && MergeCellIsAdjacent(gridTopology)
                && ColoursAreCorrect(helperFunctions)
                && NewPositionIsAllowedToMergeInRegardsToPollution(gridTopology, boardStateBefore, helperFunctions)
            )
            {
                return true;
            }
            return false;
        }

        private bool IdsAreCorrect()
        {
            if
            (
                Droplet1BeforeMove.Id != Droplet2BeforeMove.Id
                && Droplet1BeforeMove.Id != DropletAfterMove.Id
                && Droplet2BeforeMove.Id != DropletAfterMove.Id
            )
            {
                return true;
            }
            string errorMessage = 
            String.Format
            (
                "Ids are not correct for the following MergeTransition: \n {0}",
                this.WriteTransitionToTxtFormat()
            );
            Console.WriteLine(errorMessage);
            return false;
        }

        private bool VolumesAreCorrect()
        {
            if (DropletAfterMove.Volume == Droplet1BeforeMove.Volume + Droplet2BeforeMove.Volume)
            {
                return true;
            }
            string errorMessage = 
            String.Format
            (
                "Volumes are not correct for the following MergeTransition: \n {0}",
                this.WriteTransitionToTxtFormat()
            );
            Console.WriteLine(errorMessage);
            return false;
        }

        private bool MergeCellIsAdjacent( GridTopology gridTopology )
        {
            if 
            ( 
                gridTopology.N(Droplet1BeforeMove.Coordinate, Droplet1BeforeMove.Volume).Contains(DropletAfterMove.Coordinate)
                && gridTopology.N(Droplet2BeforeMove.Coordinate, Droplet2BeforeMove.Volume).Contains(DropletAfterMove.Coordinate)
            )
            {
                return true;
            }
            string errorMessage = 
            String.Format
            (
                "Merge cell is not adjacent in the following MergeTransition: \n {0}",
                this.WriteTransitionToTxtFormat()
            );
            Console.WriteLine(errorMessage);
            return false;
        }

        private bool ColoursAreCorrect( HelperFunctions helperFunctions )
        {
            if ( DropletAfterMove.Colour == helperFunctions.GetResultingColour(Droplet1BeforeMove, Droplet2BeforeMove) )
            {
                return true;
            }
            string errorMessage =
            String.Format
            (
                "Colours are not correct for the following MergeTransition: \n {0}",
                this.WriteTransitionToTxtFormat()
            );
            Console.WriteLine(errorMessage);
            return false;
        }

        private bool NewPositionIsAllowedToMergeInRegardsToPollution( GridTopology gridTopology, BoardState boardStateBefore, HelperFunctions helperFunctions )
        {
            foreach (Coordinate cell in gridTopology.NO(DropletAfterMove.Coordinate, DropletAfterMove.Volume))
            {
                if 
                (
                    !( boardStateBefore.ColourState.GetColour(cell) == 0 )
                    && !( boardStateBefore.ColourState.GetColour(cell) == Droplet1BeforeMove.Colour )
                    && !( boardStateBefore.ColourState.GetColour(cell) == Droplet2BeforeMove.Colour )
                    && !( boardStateBefore.ColourState.GetColour(cell) == helperFunctions.GetResultingColour(Droplet1BeforeMove, Droplet2BeforeMove) )
                )
                {
                    string errorMessage =
                    String.Format
                    (
                        "Droplets merge in a polluted position in the following MergeTransition: \n {0}",
                        this.WriteTransitionToTxtFormat()
                    );
                    Console.WriteLine(errorMessage);
                    return false;
                }
            }
            return true;
        }
    }

}