using System;

namespace DMFFramework
{
    class MoveTransition : ITransition
    {
        DropletState DropletBeforeMove;
        DropletState DropletAfterMove;

        public MoveTransition(DropletState dropletBeforeMove, DropletState dropletAfterMove)
        {
            DropletBeforeMove = dropletBeforeMove;
            DropletAfterMove = dropletAfterMove;
        }

        public List<DropletState> GetDropletStatesBefore()
        {
            return new List<DropletState>(1) { DropletBeforeMove };
        }

        public List<DropletState> GetDropletStatesAfter()
        {
            return new List<DropletState>(1) { DropletAfterMove };
        }

        public string WriteTransitionToTxtFormat()
        {
            string returnString = String.Format
            (
                "1#{0} | _ => {1} | _",
                DropletBeforeMove.PrintDropletState(),
                DropletAfterMove.PrintDropletState()
            );
            return returnString;
        }

        public bool IsFeasible(GridTopology gridTopology, BoardState boardStateBefore, HelperFunctions helperFunctions, IMetric metric)
        {
            if
            (
                VolumeStaysTheSame()
                && MoveIsToAnAdjacentCell(gridTopology)
                && InterferenceRegionIsClearInitially(gridTopology, boardStateBefore)
                && InterferenceRegionIsClearToMoveTo(gridTopology, boardStateBefore)
                && ColourIsTheSame()
                && NextCellNotPolluted(gridTopology, boardStateBefore)
            )
            {
                return true;
            }
            return false;
        }

// DIFFERENT FEASIBILITY CHECKS
        private bool VolumeStaysTheSame()
        {
            if ( DropletBeforeMove.Volume == DropletAfterMove.Volume )
            {
                return true;
            }
            string errorMessage = 
            String.Format
            (
                "Volumes are not correct for the following MoveTransition: \n {0}",
                this.WriteTransitionToTxtFormat()
            );
            Console.WriteLine(errorMessage);
            return false;
        }

        private bool MoveIsToAnAdjacentCell( GridTopology gridTopology )
        {
            if ( gridTopology.N(DropletBeforeMove.Coordinate, DropletBeforeMove.Volume).Contains(DropletAfterMove.Coordinate) )
            {
                return true;
            }
            string errorMessage = 
            String.Format
            (
                "Move is not to adjacent cells for the following MoveTransition: \n {0}",
                this.WriteTransitionToTxtFormat()
            );
            Console.WriteLine(errorMessage);
            return false;
        }

        private bool InterferenceRegionIsClearInitially ( GridTopology gridTopology, BoardState boardStateBefore )
        {
            foreach ( DropletState ds in boardStateBefore.DropletConfiguration.DropletStates )
            {
                if ( ds.Id != DropletBeforeMove.Id )
                {
                    if ( !gridTopology.InterferenceRegionIsClear(DropletBeforeMove, ds) )
                    {
                        string errorMessage = 
                        String.Format
                        (
                            "Initial droplets overlap in the following MoveTransition: \n {0}",
                            this.WriteTransitionToTxtFormat()
                        );
                        Console.WriteLine(errorMessage);
                        return false; 
                    }
                }
            }
            return true;
        }

        private bool InterferenceRegionIsClearToMoveTo ( GridTopology gridTopology, BoardState boardStateBefore )
        {
            foreach ( DropletState ds in boardStateBefore.DropletConfiguration.DropletStates )
            {
                if ( ds.Id != DropletBeforeMove.Id )
                {
                    if ( !gridTopology.InterferenceRegionIsClear(DropletAfterMove, ds) )
                    {
                        string errorMessage = 
                        String.Format
                        (
                            "Droplets overlap in the following MoveTransition: \n {0}",
                            this.WriteTransitionToTxtFormat()
                        );
                        Console.WriteLine(errorMessage);
                        return false; 
                    }
                }
            }
            return true;
        }

        private bool ColourIsTheSame()
        {
            if (DropletBeforeMove.Colour == DropletAfterMove.Colour)
            {
                return true;
            }
            string errorMessage = 
            String.Format
            (
                "Colours are not correct in the following MoveTransition: \n {0}",
                this.WriteTransitionToTxtFormat()
            );
            Console.WriteLine(errorMessage);
            return false;
        }

        private bool NextCellNotPolluted(GridTopology gridTopology, BoardState boardStateBefore)
        {
            foreach ( Coordinate cell in gridTopology.NO(DropletAfterMove.Coordinate, DropletAfterMove.Volume) )
                if 
                ( 
                    boardStateBefore.ColourState.GetColour(cell) != 0 
                    && boardStateBefore.ColourState.GetColour(cell) != DropletBeforeMove.Colour
                )
                {
                    string errorMessage = 
                    String.Format
                    (
                        "New position is polluted in the following MoveTransition: \n {0}",
                        this.WriteTransitionToTxtFormat()
                    );
                    Console.WriteLine(errorMessage);
                    return false;
                }
            return true;
        }

    }
}