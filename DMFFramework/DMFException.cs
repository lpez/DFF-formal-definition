using System;

namespace DMFFramework
{
    [Serializable]
    public class InfeasibleBoardStateException : Exception
    {
        public InfeasibleBoardStateException() : base() {}
        public InfeasibleBoardStateException(string message) : base(message) {}
        public InfeasibleBoardStateException(string message, Exception inner) : base(message, inner) {}
        protected InfeasibleBoardStateException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) : base(info, context) {}
    }

    [Serializable]
    public class InfeasibleStartingStateException : Exception
    {
        public InfeasibleStartingStateException() : base() {}
        public InfeasibleStartingStateException(string message) : base(message) {}
        public InfeasibleStartingStateException(string message, Exception inner) : base(message, inner) {}
        protected InfeasibleStartingStateException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) : base(info, context) {}
    }

    [Serializable]
    public class InfeasibleMoveTransitionException : Exception
    {
        public InfeasibleMoveTransitionException() : base() {}
        public InfeasibleMoveTransitionException(string message) : base(message) {}
        public InfeasibleMoveTransitionException(string message, Exception inner) : base(message, inner) {}
        protected InfeasibleMoveTransitionException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) : base(info, context) {}
    }

    [Serializable]
    public class InfeasibleMoveTogetherTransitionException : Exception
    {
        public InfeasibleMoveTogetherTransitionException() : base() {}
        public InfeasibleMoveTogetherTransitionException(string message) : base(message) {}
        public InfeasibleMoveTogetherTransitionException(string message, Exception inner) : base(message, inner) {}
        protected InfeasibleMoveTogetherTransitionException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) : base(info, context) {}
    }

    [Serializable]
    public class InfeasibleMergeInProgressTransition : Exception
    {
        public InfeasibleMergeInProgressTransition() : base() {}
        public InfeasibleMergeInProgressTransition(string message) : base(message) {}
        public InfeasibleMergeInProgressTransition(string message, Exception inner) : base(message, inner) {}
        protected InfeasibleMergeInProgressTransition(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) : base(info, context) {}
    }

    [Serializable]
    public class InfeasibleMergeTransitionException : Exception
    {
        public InfeasibleMergeTransitionException() : base() {}
        public InfeasibleMergeTransitionException(string message) : base(message) {}
        public InfeasibleMergeTransitionException(string message, Exception inner) : base(message, inner) {}
        protected InfeasibleMergeTransitionException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) : base(info, context) {}
    }
    
    [Serializable]
    public class InfeasibleSplitInProgressTransitionException : Exception
    {
        public InfeasibleSplitInProgressTransitionException() : base() {}
        public InfeasibleSplitInProgressTransitionException(string message) : base(message) {}
        public InfeasibleSplitInProgressTransitionException(string message, Exception inner) : base(message, inner) {}
        protected InfeasibleSplitInProgressTransitionException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) : base(info, context) {}
    }

    [Serializable]
    public class InfeasibleSplitTransitionException : Exception
    {
        public InfeasibleSplitTransitionException() : base() {}
        public InfeasibleSplitTransitionException(string message) : base(message) {}
        public InfeasibleSplitTransitionException(string message, Exception inner) : base(message, inner) {}
        protected InfeasibleSplitTransitionException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) : base(info, context) {}
    }
}