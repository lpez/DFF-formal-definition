using System;

namespace DMFFramework
{
    public class DropletConfiguration
    {
        public GridTopology GridTopology { get; set; }
        public List<DropletState> DropletStates { get; set; }

        public DropletConfiguration(GridTopology gridTopology, List<DropletState> dropletStates)
        {
            GridTopology = gridTopology;
            DropletStates = dropletStates;
        }

        public DropletConfiguration(GridTopology gridTopology, string solutionName, string fileName)
        {
            string filePath = "output/" + solutionName + "/txt-format/droplet-configurations/" + fileName + ".txt";
            var dropletConfigurationText = File.ReadAllText(filePath);
            string[] split = dropletConfigurationText.Split("|")[0].Split("\n", StringSplitOptions.RemoveEmptyEntries);

            GridTopology = gridTopology;
            DropletStates = (from dsString in split select new DropletState(dsString, gridTopology)).ToList();
        }

// Printing functions
        public void WriteBoardDropletIdsToTxtFormat(string solutionName, string fileName, List<DropletState> allDropletStates)
        {
            string filePath = "output/" + solutionName + "/txt-format/droplet-configurations/" + fileName + ".txt";
            StreamWriter sw = File.CreateText(filePath);
            sw.AutoFlush = true;

            // Initializes and populates the board
            var placement = new List<List<int?>>(GridTopology.Width);
            for (int i = 0; i < GridTopology.Width; i++)
            {
                placement.Add
                (
                    Enumerable.Range(0, GridTopology.Height).Select<int, int?>(x => null).ToList()
                );
            }

            foreach (DropletState dropletState in DropletStates)
            {
                foreach (Coordinate cell in GridTopology.NO(dropletState.Coordinate, dropletState.Volume))
                {
                    placement[cell.X][cell.Y] = dropletState.Id;
                }
            }

            // Writes the dropletState information to the beginning of the .txt-file
            foreach (DropletState dropletState in allDropletStates)
            {
                sw.WriteLine(dropletState.PrintDropletState());
            }
            sw.WriteLine("|");

            for (int j = 0; j < GridTopology.Height; j++)
            {
                String contentLine = "";
                for (int i = 0; i < GridTopology.Width; i++)
                {
                    int? cellContent = placement[i][j];
                    if ( cellContent is null )
                    {
                        contentLine = contentLine + "x ";
                    }
                    else
                    {
                        string cellContentString = placement[i][j].ToString();
                        contentLine = contentLine + cellContentString + " ";
                    } 
                }
                sw.WriteLine(contentLine);
            } 
        }

// FEASIBILITY CHECKS
        public bool IsFeasible()
        {
            if
            (
                HasUniqueDropletIds() 
                && EveryDropletIsInsideTheBorder()
            )
            {
                return true;
            }
            return false;
        }

        // SEPARATE CHECKS FOR FEASIBILITY
        private bool HasUniqueDropletIds()
        {
            Dictionary<int, bool> CurrentIds = new Dictionary<int, bool>(DropletStates.Count);
            bool IdHasBeenUsedAlready;
            foreach (DropletState dropletState in DropletStates)
            {
                IdHasBeenUsedAlready = !CurrentIds.TryAdd(dropletState.Id, true);
                if (IdHasBeenUsedAlready)
                {
                    string errorMessage = 
                    String.Format
                    (
                        "Two droplets exist with the same id: {0}",
                        dropletState.Id
                    );
                    Console.WriteLine(errorMessage);
                    return false;
                }
            }
            return true;
        }

        private bool EveryDropletIsInsideTheBorder()
        {
            int radius;
            foreach (DropletState dropletState in DropletStates)
            {
                radius = GridTopology.ComputeRadius(dropletState.Volume);

                if (dropletState.Coordinate.X + 1 - radius < 0){
                    string errorMessage = 
                    String.Format
                    (
                        "Droplet with id {0} lies outside the grid",
                        dropletState.Id
                    );
                    Console.WriteLine(errorMessage);
                    return false;
                }

                if (dropletState.Coordinate.X - 1 + radius > 19){
                    string errorMessage = 
                    String.Format
                    (
                        "Droplet with id {0} lies outside the grid",
                        dropletState.Id
                    );
                    Console.WriteLine(errorMessage);
                    return false;
                }

                if (dropletState.Coordinate.Y + 1 - radius < 0){
                    string errorMessage = 
                    String.Format
                    (
                        "Droplet with id {0} lies outside the grid",
                        dropletState.Id
                    );
                    Console.WriteLine(errorMessage);
                    return false;
                }

                if (dropletState.Coordinate.Y - 1 + radius > 31){
                    string errorMessage = 
                    String.Format
                    (
                        "Droplet with id {0} lies outside the grid",
                        dropletState.Id
                    );
                    Console.WriteLine(errorMessage);
                    return false;
                }
            }
            return true;
        }
    }
}